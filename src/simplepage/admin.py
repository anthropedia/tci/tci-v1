from django.contrib import admin

from .models import Page

from .forms import PageForm


class PageAdmin(admin.ModelAdmin):
    list_display = ('title', 'language')
    list_filter = ('language',)
    form = PageForm

admin.site.register(Page, PageAdmin)
