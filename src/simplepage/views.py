from django.views.generic import DetailView
from django.utils.translation import get_language

from .models import Page


class PageView(DetailView):
    model = Page

    def get_object(self):
        return self.model.objects.get_page_by_slug(
            self.kwargs['slug'], get_language)
