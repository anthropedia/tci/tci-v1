from django.test import TestCase
from django.core.urlresolvers import reverse

from .helpers import page_url

from .models import Page

from .forms import PageForm


class PageModelTest(TestCase):

    def setUp(self):
        self.page = Page.objects.create(reference='contact',
                                        title="Contact page",
                                        body="#This is a contact page")

    def test_slug_generated(self):
        p = Page.objects.create(title="My page title")
        self.assertEqual(p.slug, "my-page-title")

    def test_slug_preserved(self):
        page = Page.objects.create(title="Slugged Page")
        self.assertEqual(page.slug, "slugged-page")

    def test_slug_updated(self):
        self.page.title = "Other slugged title"
        #import ipdb; ipdb.set_trace()
        self.page.save()
        self.assertEqual(self.page.slug, "other-slugged-title")


class HelpersTest(TestCase):

    def test_simplepage_url(self):
        Page.objects.create(reference='contact', title="contact",
                            language='en')
        self.assertEqual(page_url('contact'), "/en/contact/")


class PageViewTest(TestCase):

    def setUp(self):
        self.page = Page.objects.create(reference='contact',
                                        title="Contact page",
                                        body="#This is a contact page",
                                        language='en')

    def test_render_page(self):
        r = self.client.get(reverse('simplepage:page',
                                    kwargs={'slug': "contact-page"}))
        self.assertContains(r, "<title>Contact page", 1, 200)

    def test_markdown_body(self):
        r = self.client.get(reverse('simplepage:page',
                                    kwargs={'slug': "contact-page"}))
        self.assertContains(r, "<h1>This is a contact page</h1>", 1, 200)


class PageFormTest(TestCase):

    def test_basic(self):
        data = dict(title="my title page", language="en", body="#body title")
        form = PageForm(data)
        self.assertTrue(form.is_valid())
        page = form.save()
        self.assertEqual(page.slug, "my-title-page")
