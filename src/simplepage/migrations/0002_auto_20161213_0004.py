# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('simplepage', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='page',
            name='language',
            field=models.CharField(max_length=2, choices=[(b'en', 'English'), (b'fr', 'fran\xe7ais'), (b'sw', 'Swedish')]),
            preserve_default=True,
        ),
    ]
