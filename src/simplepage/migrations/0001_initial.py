# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('reference', models.SlugField(null=True)),
                ('language', models.CharField(max_length=2, choices=[(b'en', 'English'), (b'fr', 'fran\xe7ais')])),
                ('title', models.CharField(max_length=100)),
                ('slug', models.SlugField(max_length=100)),
                ('body', models.TextField()),
                ('creation_date', models.DateField(auto_now_add=True)),
            ],
            options={
                'ordering': ('title',),
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='page',
            unique_together=set([('slug', 'language'), ('reference', 'language')]),
        ),
    ]
