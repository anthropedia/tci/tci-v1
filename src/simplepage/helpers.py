import markdown as mark
import jinja2

from django.utils.translation import get_language

from jingo import register, load_helpers
from jingo.helpers import url, fe

from .models import Page


@register.filter
def markdown(text):
    return fe(markdownize(text).get('text'))


@register.function
def page_url(reference):
    page = Page.objects.get_page_by_reference(reference, get_language())
    return url('simplepage:page', slug=page.slug)


@register.filter
@jinja2.contextfilter
def templatable(context, text):
    kwargs = dict(context)
    kwargs.update({
        'request': getattr(context.get('view'), 'request', None)
    })
    load_helpers()
    return fe(register.env.from_string(text).render(kwargs))


def markdownize(text):
    """
    returns an object with text (the html text), meta (the meta data) and
    object (the markdown instance itself).
    """
    md = mark.Markdown(extensions=['meta', 'extra', 'toc'],
                       output_format="xhtml5")
    data = {'text': md.convert(text), 'object': md}
    if hasattr(md, 'Meta'):
        data['meta'] = md.Meta
    return data
