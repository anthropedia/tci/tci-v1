from django import forms

from .models import Page


class PageForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(PageForm, self).__init__(*args, **kwargs)
        self.fields['reference'].required = False

    def clean_reference(self):
        return self.cleaned_data['reference'] or None

    class Meta:
        model = Page
        exclude = ('slug',)
