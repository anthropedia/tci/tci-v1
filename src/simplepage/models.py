from django.db import models
from django.conf import settings
from django.dispatch import receiver
from django.template.defaultfilters import slugify


class PageManager(models.Manager):

    def get_page_by_reference(self, reference, language=None):
        try:
            return self.get(reference=reference, language=language)
        except Page.DoesNotExist:
            return self.get(reference=reference)

    def get_page_by_slug(self, slug, language=None):
        try:
            return self.get(slug=slug, language=language)
        except Page.DoesNotExist:
            return self.get(slug=slug)


class Page(models.Model):
    reference = models.SlugField(max_length=50, null=True)
    language = models.CharField(max_length=2, choices=settings.LANGUAGES)
    title = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100)
    body = models.TextField()
    creation_date = models.DateField(auto_now_add=True)

    objects = PageManager()

    def __init__(self, *args, **kwargs):
        super(Page, self).__init__(* args, **kwargs)
        self._slug = self.slug

    def __str__(self):
        return str(self.title)

    class Meta:
        ordering = ('title',)
        unique_together = (('slug', 'language',), ('reference', 'language',))


@receiver(models.signals.pre_save, sender=Page)
def page_autoslug(sender, instance, **kwargs):
    if instance.title:
        instance.slug = instance.title
    instance.slug = slugify(instance.slug)
