
# Monkey patch django 1.7 that is broken on PY3
def fix_htmlparser():
    from django.utils.six.moves import html_parser
    html_parser.HTMLParseError = lambda x: x