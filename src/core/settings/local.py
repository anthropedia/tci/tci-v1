from .base import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG

INSTALLED_APPS += (
    #'debug_toolbar',
    'django_extensions',
)

MIDDLEWARE_CLASSES += (
    #'debug_toolbar.middleware.DebugToolbarMiddleware',
)

INTERNAL_IPS = ('127.0.0.1',)

DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'tci',
        'USER': 'root',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler'
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'django.db.backends': {
            'handlers': ['console'],
            'level': 'ERROR',
        },
        'shop.payment': {
            'handlers': ['console'],
            'level': 'DEBUG',
        }
    }
}

ALLOWED_HOSTS = ['*']

ABS_URL = "http://localhost:8000"
ABS_STATIC_URL = "%s%s" % (ABS_URL, STATIC_URL)

WKHTMLTOPDF_CMD_OPTIONS.update({
    'quiet': None,
})

STRIPE_SECRET_KEY = "sk_test_Dzp1uKhNEGDyqOEZuBg7ssKH"
STRIPE_PUBLIC_KEY = "pk_test_NWiEiwM3teAiCXrjtx5NX7pz"

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
