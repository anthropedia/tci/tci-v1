# -*- coding: utf-8 -*-
import os


try:
    from . import private
except ImportError:
    raise Exception('You should create a core/settings/private.py file.')


parent = os.path.dirname
BASE_DIR = parent(parent(parent(__file__)))

SECRET_KEY = 'brd@25u*%@0rq#q0m(b^2ggt!yw3cnl*zw7k3z90v&v-5qmmn#'

DEBUG = False
TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = []

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.formtools',  # WizardView for shop
    'grappelli',
    'django.contrib.admin',
    'mptt',
    'django_mptt_admin',
    'wkhtmltopdf',
    'transadmin',
    'default',
    'simplepage',
    'account',
    'survey',
    'shop',
    'analyzer',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'account.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    #'default.middlewares.ForceResponseMiddleware',
)

ROOT_URLCONF = 'core.urls'

WSGI_APPLICATION = 'core.wsgi.application'


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'data', 'default.db'),
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

LANGUAGE_CODE = 'en'

TIME_ZONE = 'America/Chicago'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'


# NON DEFAULT SETTINGS

EMAIL_FROM = "tci@anthropedia.org"

AUTH_USER_MODEL = 'account.User'

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
)

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': os.path.join(BASE_DIR, '..', 'tmp', 'django_cache'),
        'TIMEOUT': 300,
        'OPTIONS': {
            'MAX_ENTRIES': 1000
        }
    }
}


MEDIA_ROOT = ''
MEDIA_URL = ''

STATIC_ROOT = os.path.join(BASE_DIR, '..', 'assets')

HOME_DIR = "/home/anthropedia"

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'public', 'static'),
    os.path.join(BASE_DIR, '..', 'assets')
)

TEMPLATE_LOADERS = (
    'jingo.Loader',
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

LANGUAGES = (
    ('en', u'English'),
    ('fr', u'français'),
    ('sw', u'Swedish'),
)

TRANSADMIN_LANGUAGES = LANGUAGES
TRANSADMIN_FALLBACK_LANGUAGE = 'en'
TRANSADMIN_CONTEXTS = (
    ('account', "Account related"),
    ('survey', "Survey related"),
    ('shop', "Shop related"),
    ('survey_test', "Survey test (Q/A/traits)"),
)

LOGIN_URL = "/en/account/signin/"

AUTHENTICATION_BACKENDS = (
#    'account.backends.NoPasswordBackend',
#    'account.backends.InheritanceModelBackend',
    'django.contrib.auth.backends.ModelBackend',
)

JINGO_EXCLUDE_APPS = (
    #'debug_toolbar',
    'admin',
    'django_mptt_admin',
    'registration',
    'grappelli',
)

GRAPPELLI_ADMIN_TITLE = "Anthropedia TCI administration"

WKHTMLTOPDF_MAKE_ABSOLUTE_PATHS = True

WKHTMLTOPDF_CMD_OPTIONS = {
    'print-media-type': True,
}

STRIPE_SECRET_KEY = "sk_live_F8HxwlDX9JbFYJijNlgyUXQQ"
STRIPE_PUBLIC_KEY = "pk_live_CE7TkNeJYoezxtIY9UlmsdRW"
