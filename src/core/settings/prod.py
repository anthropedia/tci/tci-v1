# -*- coding: utf-8 -*-
from .base import *


DEBUG = True
TEMPLATE_DEBUG = DEBUG

STATIC_ROOT = os.path.join(HOME_DIR, 'webapps', 'tciprod_static')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': private.DB_NAME or 'tciprod',
        'USER': private.DB_USER or 'tciprod',
        'PASSWORD': private.DB_PASSWORD,
        'HOST': private.DB_HOST,
        'PORT': '',
    }
}

ALLOWED_HOSTS = ['tci.anthropedia.org']

ABS_URL = "https://tci.anthropedia.org"
ABS_STATIC_URL = "%s%s" % (ABS_URL, STATIC_URL)

EMAIL_HOST = 'smtp.webfaction.com'
EMAIL_HOST_USER = 'anthropedia_tci'
EMAIL_HOST_PASSWORD = private.EMAIL_PASSWORD
DEFAULT_FROM_EMAIL = EMAIL_FROM
SERVER_EMAIL = 'info@anthropedia.org'

WKHTMLTOPDF_CMD = '/home/anthropedia/sources/wkhtmltox/bin/wkhtmltopdf'
