# -*- coding: utf-8 -*-
from .base import *


DEBUG = True
TEMPLATE_DEBUG = DEBUG

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

STATIC_ROOT = os.path.join(HOME_DIR, 'webapps', 'tcistaging_static')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'tcistaging',
        'USER': 'tcistaging',
        'PASSWORD': private.DATABASES_DEFAULT_PASSWORD,
        'HOST': '',
        'PORT': '',
    }
}

ALLOWED_HOSTS = ['staging.tci.anthropedia.org']

ABS_URL = "https://staging.tci.anthropedia.org"
ABS_STATIC_URL = "%s%s" % (ABS_URL, STATIC_URL)

STRIPE_SECRET_KEY = "sk_test_Dzp1uKhNEGDyqOEZuBg7ssKH"
STRIPE_PUBLIC_KEY = "pk_test_NWiEiwM3teAiCXrjtx5NX7pz"

EMAIL_HOST = 'smtp.webfaction.com'
EMAIL_HOST_USER = 'anthropedia_tci'
EMAIL_HOST_PASSWORD = private.EMAIL_PASSWORD
DEFAULT_FROM_EMAIL = EMAIL_FROM
SERVER_EMAIL = 'info@anthropedia.org'
