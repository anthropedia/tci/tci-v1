from django.contrib import admin
from django.conf.urls import patterns, include, url, handler404, handler500
from django.conf.urls.i18n import i18n_patterns
from django.views.generic import RedirectView
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from default import views as default_views
from simplepage import views as simplepage_views
from account import views as account_views
from survey import views as survey_views
from shop import views as shop_views
from analyzer import views as analyzer_views
from api import views as api_views


admin.autodiscover()

# Jingo monkeypatch. See https://github.com/jbalogh/jingo#forms
#import jingo.monkey
#jingo.monkey.patch()

account_patterns = patterns('',
    # Dashboards
    url(r'^$', account_views.HomeView.as_view(), name='home'),  # redirect home
    url(r'^client/$', account_views.PatientHomeView.as_view(),
        name='patient_home'),
    url(r'^coach/$', account_views.CoachHomeView.as_view(), name='coach_home'),
    url(r'^clinician/$', account_views.ClinicianHomeView.as_view(),
        name='clinician_home'),
    url(r'^researcher/$', account_views.ResearcherHomeView.as_view(),
        name='researcher_home'),
    # Crud Patient
    url(r'^coach/client/create/$', account_views.PatientCreateView.as_view(),
        name='patient_create'),
    url(r'^coach/client/(?P<pk>\d+)/edit/$',
        account_views.PatientUpdateView.as_view(),
        name='patient_edit'),
    # Authentication
    url(r'^password/send/$', account_views.PasswordSendView.as_view(), name='password_send'),
    url(r'^password/reset/(?P<token>\w+)$', account_views.PasswordResetView.as_view(), name='password_reset'),
    url(r'^signin/$', account_views.SigninView.as_view(), name='signin'),
    url(r'^signout/$', account_views.SignoutView.as_view(), name='signout'),
)

analyzer_patterns = patterns('',
    # Coach scores
    url(r'^(?P<pk>\d+)/1d/$', analyzer_views.OneDView.as_view(), name="1d"),
    url(r'^(?P<pk>\d+)/1d.pdf$', analyzer_views.OneDPDFView.as_view(), name="1d_pdf"),
    url(r'^(?P<pk>\d+)/2d/$', analyzer_views.TwoDView.as_view(), name="2d"),
    url(r'^(?P<pk>\d+)/2d.pdf$', analyzer_views.TwoDPDFView.as_view(), name="2d_pdf"),
    url(r'^(?P<pk>\d+)/3d/$', analyzer_views.ThreeDView.as_view(), name="3d"),
    url(r'^(?P<pk>\d+)/3d.pdf$', analyzer_views.ThreeDPDFView.as_view(), name="3d_pdf"),
    # Clinician scores
    url(r'^(?P<pk>\d+)/summary/$', analyzer_views.SummaryView.as_view(),
        name='summary'),
    url(r'^(?P<pk>\d+)/summary.pdf$', analyzer_views.SummaryPDFView.as_view(),
        name='summary_pdf'),
    # Guidelines
    url(r'^guideline/(?P<version>\w+)/$', analyzer_views.GuidelineView.as_view(),
        name='guideline'),
    url(r'^guideline/(?P<version>\w+)\.pdf$', analyzer_views.GuidelinePDFView.as_view(),
        name='guideline_pdf'),
    url(r'^download/$', analyzer_views.ScoreListDownload.as_view(), name='download'),
)

api_patterns = patterns('',
    url(r'^norms/$', api_views.NormsView.as_view()),
    url(r'^percentiles/$', api_views.PercentilesView.as_view()),
    url(r'^credit/check/$', api_views.TokenCreditView.as_view()),
    url(r'^login/$', api_views.UserView.as_view()),
)

survey_patterns = patterns('',
    # Run a survey
    url(r'^test/assignment/(?P<pk>\d+)/$',
        survey_views.SurveyRunView.as_view(), name='survey_run'),
    # User survey
    url(r'^coach/client/assign/$', survey_views.AssignPatientView.as_view(),
        name='assign_patient'),
)

shop_patterns = patterns('',
    # Buy surveys
    url(r'^survey/done/$', shop_views.PurchaseDoneView.as_view(), name='survey_done'),
    url(r'^survey/', shop_views.SurveyPurchaseView.as_view(), name='survey'),
    url(r'^invoice/(?P<pk>\d+)/$', shop_views.InvoiceView.as_view(), name='invoice'),
)

simplepage_patterns = patterns('',
    url(r'^(?P<slug>[\w-]+)/$', simplepage_views.PageView.as_view(),
        name='page'),
)

urlpatterns = i18n_patterns('',
    # Specific patterns
    url(r'^$', default_views.HomeView.as_view(), name='home'),
    url(r'^account/', include(account_patterns, 'account')),
    url(r'^survey/', include(survey_patterns, 'survey')),
    url(r'^shop/', include(shop_patterns, 'shop')),
    url(r'^analyze/', include(analyzer_patterns, 'analyzer')),
    url(r'', include(simplepage_patterns, 'simplepage')),
)

urlpatterns += patterns('',
    url(r'^$', RedirectView.as_view(url='/fr/', permanent=False)),
#    url(r'^404/$', TemplateView.as_view(template_name="404.html")),
#    url(r'^500/$', TemplateView.as_view(template_name="500.html")),
    url(r'^api/', include(api_patterns, 'api')),
#    # Django patterns
    url(r'^admin/', include(admin.site.urls)),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^language/', include('django.conf.urls.i18n')),
)

urlpatterns += staticfiles_urlpatterns('static/')
