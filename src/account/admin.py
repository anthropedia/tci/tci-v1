from django.contrib import admin
from django.contrib.auth import get_user_model

from shop.models import SurveyCredit, TokenCredit

from .models import (CoachProfile, ClinicianProfile, ResearcherProfile,
                     PatientProfile, Certification)
from default import forms
from default.widgets import Display


class TokenCreditAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(TokenCreditAdminForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = 'Research title'
        self.fields['key'].widget = Display()
        self.fields['key'].required = False
        self.fields['usage_date'].widget = Display()
        self.fields['usage_date'].required = False

    class Meta:
        model = TokenCredit
        #fields = ['type', 'name', 'price', 'key', 'comment', 'usage_date']


class BaseStackedInline(admin.StackedInline):
    extra = 0
    max_num = 1
    form = forms.ModelForm


class CoachProfileInline(BaseStackedInline):
    model = CoachProfile


class ClinicianProfileInline(BaseStackedInline):
    model = ClinicianProfile


class PatientProfileInline(BaseStackedInline):
    model = PatientProfile
    fk_name = 'user'


class SurveyCreditInline(admin.TabularInline):
    model = SurveyCredit
    extra = 0


class TokenCreditInline(admin.TabularInline):
    model = TokenCredit
    fields = ['type', 'name', 'price', 'comment', 'key', 'usage_date']
    readonly_fields = ['usage_date', 'key']
    # form = TokenCreditAdminForm
    extra = 0


class ResearcherProfileInline(BaseStackedInline):
    model = ResearcherProfile


class UserAdminForm(forms.EditablePasswordFormMixin, forms.ModelForm):
    pass


class UserAdmin(admin.ModelAdmin):
    form = UserAdminForm
    list_display = ['__str__', 'profiles_data', 'creation_date']
    search_fields = ('email', 'firstname', 'lastname')
    # form
    readonly_fields = ('last_login',)
    fieldsets = [
        (None, {'fields': ('email', 'firstname', 'lastname', 'language',)}),
        ('Authentication', {'fields': ("password",)}),
        ('Infos', {'fields': ("last_login",)}),
    ]
    inlines = [CoachProfileInline, ClinicianProfileInline,
               ResearcherProfileInline, PatientProfileInline,
               SurveyCreditInline, TokenCreditInline]

admin.site.register([Certification])
admin.site.register(get_user_model(), UserAdmin)
