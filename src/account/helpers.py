# -*- coding: utf-8 -*-

import jinja2

from jingo import register

from . import constants
from .forms import PasswordResetForm


@register.filter
def usertype_label(usertype):
    return dict(constants.USER_TYPE).get(usertype)


@register.function
def password_reset(request):
    form = PasswordResetForm(request.POST or None)
    return {
        'form': form
    }
