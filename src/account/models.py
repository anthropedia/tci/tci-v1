from django.conf import settings
from django.db import models
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.contrib.auth.models import (AbstractBaseUser, BaseUserManager,
                                        PermissionsMixin)
from django.utils.crypto import get_random_string

from django_countries.fields import CountryField
from django_localflavor_us.models import USStateField

from jsonfield import JSONField

from default import _
from default.utils import calculate_age

from . import constants
from . import permissions


PROFESSIONAL_ROLES = ('coach', 'clinician', 'researcher',)


class Certification(models.Model):

    """
    Certifications for coach
    """
    title = models.CharField(max_length=50)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['title']


class UserManager(BaseUserManager):

    def _create_user(self, email, password, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        now = timezone.now()
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, is_superuser=is_superuser,
                          is_active=True, last_login=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        return self._create_user(email, password, False, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, **extra_fields)


class ExtendedUser(object):
    """
    A non database user that is used for the request and has convenient
    methods.
    """

    # A session object is added in account.middleware.AuthenticationMiddleware.
    session = None

    def change_profile(self, request, profile_name):
        request.session['user.profile_name'] = profile_name

    @property
    def profile(self):
        """
        This should return the current profile of the session user based on the
        'user.profile_name' session value.
        """
        profile_name = self.session.get('user.profile_name')
        if profile_name:
            return getattr(self, "%s_profile" % profile_name)
        return None

    @property
    def credits(self):
        return self.credit_set.all()

    @property
    def results(self):
        return self.result_set.all()

    class Meta:
        proxy = True
        managed = False
        verbose_name = "User"
        swappable = 'AUTH_USER_MODEL'


class User(AbstractBaseUser, PermissionsMixin, ExtendedUser):
    TOKEN_SIZE = 32
    email = models.EmailField(_('email address'), unique=True)
    is_active = models.BooleanField(_('active'), default=True)
    firstname = models.CharField(_('first name'), max_length=30, blank=True)
    lastname = models.CharField(_('last name'), max_length=30, blank=True)
    language = models.CharField(max_length=2, choices=settings.LANGUAGES,
                                null=True, default=settings.LANGUAGES[0][0])
    profiles_data = models.CharField(max_length=100, null=True)
    creation_date = models.DateField(auto_now_add=True)
    token = models.CharField(max_length=TOKEN_SIZE)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    @property
    def is_staff(self):
        return self.is_superuser

    def get_short_name(self):
        if self.firstname and self.lastname:
            return "%s %s" % (self.firstname[0], self.lastname)
        else:
            return str(self)

    def __str__(self):
        if self.firstname and self.lastname:
            display = "%s %s" % (self.firstname, self.lastname)
        elif self.email:
            display = self.email
        else:
            display = super(User, self).__str__()
        if not display.strip():
            display = "Unknown user"
        return display

    def add_profile(self, profile):
        profiles = self.profiles
        profiles.append(profile)
        self.profiles_data = ','.join(list(set(profiles)))

    def remove_profile(self, profile):
        profiles = self.profiles
        if profile in profiles:
            profiles.remove(profile)
        self.profiles_data = ','.join(profiles)

    @property
    def currency(self):
        profiles = self.profiles
        if len(profiles):
            return getattr(self, "%s_profile" % self.profiles[0]).currency

    @property
    def profiles(self):
        if self.profiles_data:
            profs = self.profiles_data.split(',')
            # make the 'patient' profile always last
            if 'patient' in profs:
                profs.append(profs.pop(profs.index('patient')))
            return profs
        else:
            return []

    def has_perm(self, perm, obj=None):
        if self.is_superuser:
            return True
        profiles = self.profiles
        perms = ()
        for p in profiles:
            perms += getattr(permissions, '%s_perms' % p)
        return perm in perms

    def reset_token(self):
        self.token = get_random_string(self.TOKEN_SIZE)

    def check_token(self, token):
        return (len(token) == self.TOKEN_SIZE and
                self.token == token)

    def save(self, *args, **kwargs):
        if not self.token:
            self.reset_token()
        return super(User, self).save(*args, **kwargs)

    class Meta:
        ordering = ['lastname']


class BaseProfile(models.Model):
    street = models.CharField(max_length=50, null=True)
    street2 = models.CharField(max_length=50, null=True)
    city = models.CharField(max_length=50, null=True)
    state = USStateField(null=True)
    postal_code = models.CharField(max_length=10, null=True)
    country = CountryField(null=True)
    creation_date = models.DateField(auto_now_add=True)
    extra = JSONField(null=True)

    @property
    def name(self):
        return "%s %s" % (self.user.firstname, self.user.lastname)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        """
        When creating a profile the user should cache it in it's `profiles`
        property.
        """
        if not hasattr(self, 'user'):
            self.user = get_user_model().objects.create()
        self.user.add_profile(self.profile_type)
        self.user.save()
        return super(BaseProfile, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self.user.remove_profile(self.profile_type)
        self.user.save()
        return super(BaseProfile, self).delete(*args, **kwargs)

    def __str__(self):
        return str(self.user)


class BaseProfessionalProfile(BaseProfile):
    currency = models.CharField(max_length=3, choices=(constants.CURRENCIES),
                                default=constants.CURRENCIES[0][0])

    @property
    def patients(self):
        return PatientProfile.objects.filter(
            models.Q(professional=self.user) | models.Q(user=self.user))

    @property
    def orders(self):
        return self.user.order_set.all()

    class Meta:
        abstract = True


class CoachProfile(BaseProfessionalProfile):
    profile_type = 'coach'
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                related_name='coach_profile')
    certification = models.ForeignKey(Certification, null=True,
                                      on_delete=models.SET_NULL)


class ClinicianProfile(BaseProfessionalProfile):
    profile_type = 'clinician'
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                related_name='clinician_profile')


class ResearcherProfile(BaseProfessionalProfile):
    profile_type = 'researcher'
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                related_name='researcher_profile')


class PatientProfileManager(models.Manager):
    def create_from_professional(self, professional):
        return self.get_or_create(professional=professional)


class PatientProfile(BaseProfile):
    profile_type = 'patient'
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                related_name='patient_profile')
    professional = models.ForeignKey(settings.AUTH_USER_MODEL, null=True,
                                     related_name='patient_set')
    professional_role = models.CharField(max_length=20, null=True, blank=True,
                                         choices=zip(PROFESSIONAL_ROLES,
                                                     PROFESSIONAL_ROLES))
    reference = models.CharField(max_length=50, null=True)
    occupation = models.CharField(max_length=50, null=True)
    gender = models.CharField(max_length=1, null=True,
                              choices=constants.GENDER)
    birth_date = models.DateField(null=True)
    survey_language = models.CharField(
        max_length=2, choices=settings.LANGUAGES, default='en',
        help_text=_("account_survey_language.help"))
    survey_country = CountryField(default='US',
                                  help_text=_("account_survey_country.help"))
    education_level = models.CharField(max_length=20, null=True,
                                       choices=constants.EDUCATION)
    ethnicity = models.CharField(max_length=20, null=True,
                                 choices=constants.ETHNICITY)
    marital_status = models.CharField(max_length=20, null=True,
                                      choices=constants.MARITAL)

    objects = PatientProfileManager()

    @property
    def age(self):
        return calculate_age(self.birth_date)

    def __str__(self):
        return str(self.user)


### For compatibility purpose only

class UserGroups(models.Model):
    class Meta:
        pass  # db_table = "account_user_groups"


class UserPermissions(models.Model):
    class Meta:
        pass  # db_table = "account_user_user_permissions"
