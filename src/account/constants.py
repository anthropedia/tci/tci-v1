from default import _


USER_TYPE = (
    ('coach', _("Coach")),
    ('researcher', _("Researcher")),
    ('patient', _("Client")),
    ('individual', _("Individual")),
)

ETHNICITY = (
    ('caucasian', _("Caucasian")),
    ('hispanic', _("Latino/Hispanic")),
    ('middle Eastern', _("Middle Eastern")),
    ('african', _("African")),
    ('caribbean', _("Caribbean")),
    ('south-asian', _("South Asian")),
    ('east-asian', _("East Asian")),
    ('mixed', _("Mixed")),
    ('other', _("Other")),
)

EDUCATION = (
    ('high-school-less', _("Some high school or less")),
    ('High-school', _("High school graduate")),
    ('college', _("Some college")),
    ('associate', _("Associate's degree")),
    ('bachelor', _("Bachelor's degree")),
    ('master', _("Master's degree")),
    ('professional', _("Professional school degree")),
    ('doctorate', _("Doctorate degree")),
)

MARITAL = (
    ('married', _("Now married")),
    ('widowed', _("Widowed")),
    ('divorced', _("Divorced")),
    ('separated', _("Separated")),
    ('unmarried', _("Never married")),
)

GENDER = (
    ('m', _("Male")),
    ('f', _("Female")),
)

CURRENCIES = (
    ('usd', "US Dollars"),
    ('eur', "Euros"),
)
