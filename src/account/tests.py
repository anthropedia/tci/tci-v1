from django.test import TestCase

from .models import User, CoachProfile, PatientProfile

from .forms import PatientUserForm


class ProfileCreationTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(email='user@tci.test')
        PatientProfile.objects.create(user=self.user)

    def test_profile_added(self):
        self.assertEquals(self.user.profiles, ['patient'])

    def test_multiple_profiles_added(self):
        CoachProfile.objects.create(user=self.user)
        self.assertListEqual(self.user.profiles, ['coach', 'patient'])

    def test_profile_deleted(self):
        CoachProfile.objects.create(user=self.user)
        assert 'coach' in self.user.profiles
        self.user.coach_profile.delete()
        self.assertListEqual(self.user.profiles, ['patient'])


class ProfileFormTest(TestCase):

    def test_creating_patient_also_creates_user(self):
        coach = User.objects.create(email="coach@test.tmp")
        params = {
            'data': {
                'email': "johndoe@test.tmp",
                'password': "johndoe",
                'firstname': "John",
                'lastname': "Doe",
                'country': "CA",
                'street': "main st.",
            },

            'extra': {
                'professional': coach,
                'professional_relation': "coach"
            }
        }
        form = PatientUserForm(**params)
        patient = form.save()
        self.assertIsInstance(patient, PatientProfile)
        self.assertEqual(patient.street, "main st.")
        self.assertIsInstance(patient.user, User)
        self.assertEqual(patient.user.email, "johndoe@test.tmp")
        self.assertEqual(patient.professional, coach)


class UserPermissionsTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(email='user@tci.test')
        PatientProfile.objects.create(user=self.user)

    def test_has_perm(self):
        self.assertFalse(self.user.has_perm('screw.the.world'))
        self.assertFalse(self.user.has_perm('shop.survey'))
        self.assertTrue(self.user.has_perm('dashboard'))
