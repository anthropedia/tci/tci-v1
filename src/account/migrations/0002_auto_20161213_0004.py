# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='patientprofile',
            name='survey_language',
            field=models.CharField(default=b'en', help_text=b'account_survey_language.help', max_length=2, choices=[(b'en', 'English'), (b'fr', 'fran\xe7ais'), (b'sw', 'Swedish')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='user',
            name='language',
            field=models.CharField(default=b'en', max_length=2, null=True, choices=[(b'en', 'English'), (b'fr', 'fran\xe7ais'), (b'sw', 'Swedish')]),
            preserve_default=True,
        ),
    ]
