class UserAdminCompatibleMixin(object):
    """
    These properties make apps such as Grappelli compatible
    """
    @property
    def is_active(self):
        return True

    @property
    def is_staff(self):
        return self.is_superuser

    @property
    def first_name(self):
        return self.email

    @property
    def username(self):
        return self.email

    @property
    def short_name(self):
        return self.email