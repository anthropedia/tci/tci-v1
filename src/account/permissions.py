
"""
Staticly gives permissions to users by their role (Coach, Patient, ...)
"""

_professional_perms = ('dashboard', 'patient.update',
                       'professional.assign_patient', 'shop.survey',
                       'survey.result')
coach_perms = _professional_perms
clinician_perms = _professional_perms
researcher_perms = _professional_perms
patient_perms = ('dashboard', 'survey.run', 'survey.result')
individual_perms = ('dashboard', 'survey.run', 'survey.result')
