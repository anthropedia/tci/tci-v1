from django.contrib.auth import authenticate

from django_countries import countries

from default import forms
from default import _

from .models import User
from .models import PatientProfile

COUNTRIES = countries.countries


class SigninForm(forms.Form):
    error_messages = {
        'invalid_login': _("Please enter a correct %(username)s and password. "
                           "Note that both fields may be case-sensitive."),
    }
    email = forms.EmailField(required=True)
    password = forms.CharField(required=True, widget=forms.PasswordInput)
    user = None

    def clean(self):
        valid = True
        try:
            self.user = authenticate(email=self.data['email'],
                                     password=self.data['password'])
        except forms.ValidationError:
            valid = False
        if not valid or not self.user:
            raise forms.ValidationError(self.error_messages['invalid_login'] %
                                        {'username': "email"})


class UserForm(forms.EditablePasswordFormMixin, forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)

    class Meta:
        model = User
        fields = ['email', 'password', 'firstname', 'lastname', 'language']


class PatientUserForm(forms.MixedModelForm):

    def __init__(self, *args, **kwargs):
        super(PatientUserForm, self).__init__(*args, **kwargs)
        self.fields['language'].label = 'Website language'

    def save(self, *args, **kwargs):
        self.instance.professional = self.extra['professional']
        self.instance.professional_relation = (
            self.extra['professional_relation'])
        return super(PatientUserForm, self).save(*args, **kwargs)

    class Meta:
        required = ['survey_country', 'survey_language']
        model = PatientProfile
        extra_forms = [(UserForm, 'user')]
        fields = ['reference', 'gender', 'street', 'street2', 'postal_code',
                  'city', 'state', 'country', 'survey_country',
                  'survey_language', 'occupation', 'birth_date',
                  'education_level', 'ethnicity', 'marital_status']


class PatientUserCreateForm(PatientUserForm):

    def __init__(self, *args, **kwargs):
        super(PatientUserCreateForm, self).__init__(*args, **kwargs)
        self.fields['password'].required = True


class PasswordResetForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput(), required=True)

    def save(self):
        self.user.set_password(self.cleaned_data['password'])
        self.user.reset_token()
        self.user.save()
        return self.user
