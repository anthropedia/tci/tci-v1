from django.utils.functional import SimpleLazyObject
from django.contrib import auth


def get_user(request):
    """
    Override the django method to add a `session` property to the current user.
    """
    if not hasattr(request, '_cached_user'):
        request._cached_user = auth.get_user(request)
        request._cached_user.session = request.session
    return request._cached_user


class AuthenticationMiddleware(object):
    """
    Override this Middleware to allow the user to have a `session` property.
    """
    def process_request(self, request):
        assert hasattr(request, 'session'), (
            "The Django authentication middleware requires session middleware "
            "to be installed. Edit your MIDDLEWARE_CLASSES setting to insert "
            "'django.contrib.sessions.middleware.SessionMiddleware' before "
            "'django.contrib.auth.middleware.AuthenticationMiddleware'."
        )
        request.user = SimpleLazyObject(lambda: get_user(request))
