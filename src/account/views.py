from django.views.generic import (FormView, RedirectView, TemplateView,
                                  CreateView, UpdateView)
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib.auth.decorators import permission_required
from django.contrib.auth import login, logout
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.utils.http import urlquote
from django.core.mail import send_mail
from django.conf import settings

from default.decorators import view_decorator
from default import _
from default.helpers import abs_url
from survey.models import Survey, UserResult

from .forms import (SigninForm, PatientUserForm, PatientUserCreateForm,
                    PasswordResetForm)
from .models import PROFESSIONAL_ROLES, PatientProfile, User


class SigninView(FormView):
    form_class = SigninForm
    template_name = "account/signin.html"
    success_url = reverse_lazy('account:home')

    def post(self, request, *args, **kwargs):
        """
        If we receive a request for the password_reset we should redirect.
        """
        if 'password-reset' in request.POST:
            return HttpResponseRedirect("%s?email=%s" % (
                reverse('account:password_send'),
                urlquote(request.POST.get('email'))))
        return super(SigninView, self).post(request, *args, **kwargs)

    def form_valid(self, form):
        login(self.request, form.user)
        return super(SigninView, self).form_valid(form)


class SignoutView(RedirectView):
    permanent = False
    url = reverse_lazy('home')

    def get(self, *args, **kwargs):
        logout(self.request)
        return super(SignoutView, self).get(*args, **kwargs)


#@view_decorator(permission_required('dashboard'))
class HomeView(RedirectView):
    """
    Redirects the current user to the dashboard corresponding to his profile.
    """
    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        user_profile = self.request.user.profile
        profile = user_profile.profile_type if user_profile else None
        if not profile:
            profiles = self.request.user.profiles
            # find the first
            priorities = list(PROFESSIONAL_ROLES) + ['patient']
            try:
                profile = next(p for p in priorities if p in profiles)
            except StopIteration:
                messages.error(self.request, _("message.dashboard.no_access"))
                return reverse("home")
        return reverse("account:%s_home" % profile)


class BaseProfileHomeView(TemplateView):
    """
    Abstract class that dashboards should extend.
    It sets the current user's profile to the corresponding dashboard and
    therefore toggles his profile.
    """
    def dispatch(self, request, *args, **kwargs):
        profile_name = self.__class__.__name__[:-8].lower()
        request.user.change_profile(request, profile_name)
        return (super(BaseProfileHomeView, self)
                .dispatch(request, *args, **kwargs))


@view_decorator(permission_required('dashboard'))
class PatientHomeView(BaseProfileHomeView):
    template_name = "account/home/patient.html"

    def get_context_data(self, **kwargs):
        context = super(PatientHomeView, self).get_context_data(**kwargs)
        user = self.request.user
        context.update({
            'user': user,
            'pending_result_list': user.results.incomplete(),
        })
        return context


@view_decorator(permission_required('dashboard'))
class ResearcherHomeView(BaseProfileHomeView):
    template_name = "account/home/researcher.html"

    def get_context_data(self, **kwargs):
        context = super(ResearcherHomeView, self).get_context_data(**kwargs)
        user = self.request.user
        context.update({
            'user': user,
            'survey_list': Survey.objects.all(),
            'result_list': UserResult.objects.by_professional(user),
        })
        return context


@view_decorator(permission_required('dashboard'))
class ClinicianHomeView(BaseProfileHomeView):
    template_name = "account/home/clinician.html"

    def get_context_data(self, **kwargs):
        context = super(ClinicianHomeView, self).get_context_data(**kwargs)
        user = self.request.user
        context.update({
            'user': user,
            'survey_list': Survey.objects.all(),
            'result_list': UserResult.objects.by_professional(user),
        })
        return context


@view_decorator(permission_required('dashboard'))
class CoachHomeView(BaseProfileHomeView):
    template_name = "account/home/coach.html"

    def get_context_data(self, **kwargs):
        context = super(CoachHomeView, self).get_context_data(**kwargs)
        user = self.request.user
        context.update({
            'user': user,
            'survey_list': Survey.objects.all(),
            'result_list': UserResult.objects.by_professional(user)
        })
        return context


@view_decorator(permission_required('patient.update'))
class PatientCreateView(CreateView):
    model = PatientProfile
    form_class = PatientUserCreateForm
    template_name = "account/patient_create.html"
    success_url = reverse_lazy('account:home')

    def form_valid(self, *args, **kwargs):
        messages.success(self.request, _("message.create_patient.success"))
        return super(PatientCreateView, self).form_valid(*args, **kwargs)

    def get_form_kwargs(self, **kwargs):
        kwargs = super(PatientCreateView, self).get_form_kwargs(**kwargs)
        kwargs['extra'] = {
            'professional': self.request.user,
            'professional_relation': 'coach'
        }
        return kwargs


@view_decorator(permission_required('patient.update'))
class PatientUpdateView(UpdateView):
    model = PatientProfile
    form_class = PatientUserForm
    template_name = "account/patient_update.html"
    success_url = reverse_lazy('account:home')

    def get_context_data(self, **kwargs):
        ctx = super(PatientUpdateView, self).get_context_data(**kwargs)
        ctx['result_list'] = self.object.user.results
        return ctx

    def get_form_kwargs(self, **kwargs):
        kwargs = super(PatientUpdateView, self).get_form_kwargs(**kwargs)
        kwargs['extra'] = {
            'professional': self.request.user,
            'professional_relation': 'coach'
        }
        return kwargs

    def form_valid(self, *args, **kwargs):
        messages.success(self.request, _("message.update_patient.success"))
        return super(PatientUpdateView, self).form_valid(*args, **kwargs)


class PasswordResetView(FormView):
    form_class = PasswordResetForm
    template_name = "account/password_reset.html"

    def dispatch(self, request, *args, **kwargs):
        try:
            self.user = User.objects.get(token=self.kwargs.get('token'))
        except User.DoesNotExist:
            messages.error(request, _("message.password_reset.error"))
            return HttpResponseRedirect(reverse('account:signin'))
        return super(PasswordResetView, self).dispatch(request, *args, **kwargs)

    def get_form(self, *args, **kwargs):
        form = super(PasswordResetView, self).get_form(*args, **kwargs)
        form.user = self.user
        return form

    def form_valid(self, form):
        form.save()
        messages.success(self.request, _("message.password_reset.success"))
        return HttpResponseRedirect(reverse('account:signin'))

    def form_invalid(self, *args, **kwargs):
        super(PasswordResetView, self).form_invalid(*args, **kwargs)


class PasswordSendView(RedirectView):
    url = reverse_lazy('account:signin')

    def get(self, request, *args, **kwargs):
        def message(user):
            return (_("mail.password_reset.body (reset_url)").format(
                    reset_url=abs_url(reverse('account:password_reset',
                                      kwargs={'token': user.token}))
                    ))
        try:
            # Send an email with the reset link to the user.
            user = User.objects.get(email=self.request.GET.get('email'))
            send_mail(_("mail.password_reset.subject"), message(user),
                      settings.EMAIL_FROM, [user.email])
            # Confirmation user message.
            messages.success(request, _("message.password_send.success"))
        except User.DoesNotExist:
            # Error message.
            messages.error(request, _("message.password_send.error"))
        return super(PasswordSendView, self).get(request, *args, **kwargs)
