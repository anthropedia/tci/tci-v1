from default import _

TEMPERAMENT = 'temperament'
CHARACTER = 'character'

CONSTRUCT = (
    (CHARACTER, _("Character")),
    (TEMPERAMENT, _("Temperament")),
)

TEMPERAMENT_TYPES = (
    # RD low
    (   # NS low
        (_("indifferent"), _("methodical")),  # HA low and high
        # NS high
        (_("adventurous"), _("explosive"))  # HA low and high
    ),
    # RD high
    (   # NS low
        (_("reliable"), _("cautious")),  # HA low and high
        # NS high
        (_("passionate"), _("sensitive"))  # HA low and high
    )
)

CHARACTER_TYPES = (
    # CO low
    (   # ST low
        (_("apathetic"), _("bossy")),  # SD low and high
        # ST high
        (_("disorganised"), _("absolutist"))  # SD low and high
    ),
    # CO high
    (   # ST low
        (_("dependant"), _("organized")),  # SD low and high
        # ST high
        (_("moody"), _("creative"))  # SD low and high
    )
)
