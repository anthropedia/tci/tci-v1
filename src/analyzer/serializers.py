# -*- coding: utf-8 -*-

import re
from copy import copy


class ScoreCSVSerializer(object):

    columns = [
       ('id', '.id'),
       ('client', '.name'),
       ('test', '.survey_name'),
       ('date', '.creation_date'),
    ]

    @classmethod
    def serialize(klass, instance):
        """
        Returns a dict with 'header' and 'body' lists of records
        for the instance.
        """
        output = {'header': [], 'body': []}
        pattern = re.compile('^\.[a-z()_]+$')

        for column in klass.get_columns(instance):
            output['header'].append(column[0])
            value = column[1]
            if type(value) == str and pattern.match(value):
                value = getattr(instance, value[1:])
            output['body'].append(str(value))
        return output

    @classmethod
    def get_actual_raw_answers(klass, score):
        '''
        raw_answers are irrelevant in some eratic cases.
        Let's recalculate.
        '''
        return [int(v) + 1 for v in (
            score.result.answers.order_by('question__position')
                 .values_list('answer__position', flat=True))]

    @classmethod
    def get_columns(klass, instance):
        columns = copy(klass.columns)
        answers = klass.get_actual_raw_answers(instance)
        for i, answer in enumerate(answers):
            columns.append(('Q%d' % (i + 1), answers[i]))
        for trait in instance.survey.traits:
            abbr = trait.abbreviation
            # raw score
            columns.append(
                ('%s(Avg Raw Score)' % trait.abbreviation,
                 float(instance.rawscore_averages[abbr])))
            # t score
            columns.append(
                ('%s(T Score)' % trait.abbreviation,
                 round(float(instance.tscores[abbr]))))
            # percentile
            percentile = instance.percentiles[abbr]
            columns.append(('%s(Percentile)' % trait.abbreviation, percentile))

        return columns
