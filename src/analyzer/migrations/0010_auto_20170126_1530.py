# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def translate_tci_old_traits(apps, schema_editor):
    translations = [('nv', 'ns'), ('hm', 'ha'), ('rw', 'rd'), ('pr', 'ps'),
                    ('dr', 'sd'), ('cp', 'co'), ('tr', 'st'), ('se', 'st')]
    trans = dict(translations)
    properties = ('rawscores', 'rawscore_averages', 'rawscore_percentages',
                  'personality_percentages', 'rawscore_qualitatives',
                  'zscores', 'tscores', 'percentiles')
    from analyzer.models import Score
    db_alias = schema_editor.connection.alias
    for score in Score.objects.using(db_alias).all():
        for prop in properties:
            obj = getattr(score, prop)
            if not type(obj) == dict:
                continue
            for k, v in getattr(score, prop).items():
                old_trait = k[:2]
                if old_trait in trans:
                    new_key = k.replace(old_trait, trans[old_trait])
                    obj[new_key] = v
                    del obj[k]
        score.save(update_fields=list(properties))

class Migration(migrations.Migration):

    dependencies = [
        ('analyzer', '0009_auto_20170126_1529'),
    ]

    operations = [
        migrations.RunPython(translate_tci_old_traits),
    ]
