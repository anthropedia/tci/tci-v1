# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analyzer', '0005_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='score',
            name='result',
            field=models.OneToOneField(related_name='score', default='', to='survey.UserResult'),
            preserve_default=False,
        ),
    ]
