# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields
import django_countries.fields


class Migration(migrations.Migration):

    dependencies = [
        ('survey', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Score',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('survey_name', models.CharField(max_length=100)),
                ('name', models.CharField(max_length=100, null=True)),
                ('reference', models.CharField(max_length=100, null=True)),
                ('gender', models.CharField(max_length=1, null=True, choices=[(b'm', b'Male'), (b'f', b'Female')])),
                ('country', django_countries.fields.CountryField(max_length=2, null=True)),
                ('education_level', models.CharField(max_length=20, null=True, choices=[(b'high-school-less', b'Some high school or less'), (b'High-school', b'High school graduate'), (b'college', b'Some college'), (b'associate', b"Associate's degree"), (b'bachelor', b"Bachelor's degree"), (b'master', b"Master's degree"), (b'professional', b'Professional school degree'), (b'doctorate', b'Doctorate degree')])),
                ('ethnicity', models.CharField(max_length=20, null=True, choices=[(b'caucasian', b'Caucasian'), (b'hispanic', b'Latino/Hispanic'), (b'middle Eastern', b'Middle Eastern'), (b'african', b'African'), (b'caribbean', b'Caribbean'), (b'south-asian', b'South Asian'), (b'east-asian', b'East Asian'), (b'mixed', b'Mixed'), (b'other', b'Other')])),
                ('marital_status', models.CharField(max_length=20, null=True, choices=[(b'married', b'Now married'), (b'widowed', b'Widowed'), (b'divorced', b'Divorced'), (b'separated', b'Separated'), (b'unmarried', b'Never married')])),
                ('age', models.PositiveSmallIntegerField(null=True)),
                ('raw_answers', jsonfield.fields.JSONField()),
                ('answer_values', jsonfield.fields.JSONField()),
                ('validity_questions', jsonfield.fields.JSONField()),
                ('rawscores', jsonfield.fields.JSONField()),
                ('rawscore_averages', jsonfield.fields.JSONField()),
                ('rawscore_percentages', jsonfield.fields.JSONField()),
                ('rawscore_qualitatives', jsonfield.fields.JSONField()),
                ('zscores', jsonfield.fields.JSONField()),
                ('tscores', jsonfield.fields.JSONField()),
                ('percentiles', jsonfield.fields.JSONField()),
                ('extra', jsonfield.fields.JSONField()),
                ('creation_date', models.DateField(auto_now_add=True)),
                ('result', models.OneToOneField(related_name='score', null=True, to='survey.UserResult')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
