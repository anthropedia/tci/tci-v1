# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

from analyzer.models import RawscoreCalculator


class Migration(migrations.Migration):

    dependencies = [
        ('analyzer', '0002_auto_20160816_1557'),
    ]
