# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analyzer', '0004_generate_personality_percentages'),
        ('analyzer', '0003_generate_personality_percentages'),
    ]

    operations = [
    ]
