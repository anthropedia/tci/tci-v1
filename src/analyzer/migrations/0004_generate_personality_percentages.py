# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

from analyzer.models import RawscoreCalculator


def generate_personality_percentages(apps, schema_editor):
    # Score = apps.get_model('analyzer', 'Score')
    # We're using the actual Score instance as we need access to some virtual
    # @property such as `traits`
    from analyzer.models import Score
    db_alias = schema_editor.connection.alias
    # for score in Score.objects.using(db_alias).all():
    #     new_score = Score.generator.from_result(score.result)
    #     new_score.pk = score.pk
    #     new_score.save()
    #     score.delete()
    for score in Score.objects.using(db_alias).all():
        calculator = RawscoreCalculator(score.result)
        score.personality_percentages = calculator.personality_percentages()
        score.trait_percentages = calculator.trait_percentages()
        score.rawscore_qualitatives = calculator.qualitatives()
        score.rawscore_averages = calculator.averages()
        score.save(update_fields=['personality_percentages',
                                  'rawscore_averages',
                                  'rawscore_qualitatives',
                                  'rawscore_averages'])


class Migration(migrations.Migration):

    dependencies = [
        ('analyzer', '0002_auto_20160816_1557'),
        ('survey', '0003_trait_label'),
    ]

    operations = [
        migrations.RunPython(generate_personality_percentages),
    ]
