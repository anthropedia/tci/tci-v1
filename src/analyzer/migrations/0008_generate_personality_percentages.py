# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

from analyzer.models import RawscoreCalculator


def generate_personality_percentages(apps, schema_editor):
    from analyzer.models import Score
    db_alias = schema_editor.connection.alias
    for score in Score.objects.using(db_alias).all():
        calculator = RawscoreCalculator(score.result)
        score.personality_percentages = calculator.personality_percentages()
        score.save(update_fields=['personality_percentages'])


class Migration(migrations.Migration):

    dependencies = [
        ('analyzer', '0007_generate_personality_percentages'),
    ]

    operations = [
        migrations.RunPython(generate_personality_percentages),
    ]
