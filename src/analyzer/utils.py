from analyzer.constants import TEMPERAMENT, CHARACTER
from .constants import TEMPERAMENT, CHARACTER


def personality_type(abbr):
    return TEMPERAMENT if abbr[:2] in ['ha', 'ns', 'rd', 'ps'] else CHARACTER


def linearize_rawscore(abbr, value):
    """Rebase a rawscore to its actual personality value based on its type
    """
    if personality_type(abbr) == TEMPERAMENT:
        return abs(3 - value) * 2 + 1
    return value
