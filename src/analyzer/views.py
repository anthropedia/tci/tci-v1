import csv

from django.views.generic import DetailView, TemplateView, ListView
from django.http import HttpResponse
from django.shortcuts import redirect

from wkhtmltopdf.views import PDFTemplateView

from .serializers import ScoreCSVSerializer
from .models import Score

class PDFTemplateView(TemplateView):
    pass

class PDFDetailView(PDFTemplateView):
    context_object_name = 'object'
    show_content_in_browser = False
    queryset = None

    def get(self, request, *args, **kwargs):
        # @todo: for now a pdf rendering is called to the outside on port 3003, that's temporary and should be improved
        return redirect("https://html2pdf.anthropedia.org/?url=%s&name=%s" % (request.build_absolute_uri().replace(".pdf", ""), self.get_filename()))
        self.object = self.get_object()
        return super(PDFDetailView, self).get(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(PDFDetailView, self).get_context_data(*args, **kwargs)
        context[self.context_object_name] = self.get_object()
        return context

    def get_filename(self):
        return "%s-%s.pdf" % (
            self.get_context_data()[self.context_object_name],
            self.version)

    def get_object(self):
        q = self.queryset or Score.objects
        return q.get(pk=self.kwargs['pk'])


class OneDView(DetailView):
    model = Score
    template_name = "analyzer/1d.html"


class OneDPDFView(PDFDetailView):
    template_name = OneDView.template_name
    model = OneDView.model
    context_object_name = 'score'
    version = '1d'
    cmd_options = {
        'orientation': 'Portrait',
    }


class TwoDView(DetailView):
    model = Score
    template_name = "analyzer/2d.html"


class TwoDPDFView(PDFDetailView):
    template_name = TwoDView.template_name
    model = TwoDView.model
    context_object_name = 'score'
    version = '2d'
    cmd_options = {
        'orientation': 'Portrait',
    }


class ThreeDView(DetailView):
    model = Score
    template_name = "analyzer/3d.html"


class ThreeDPDFView(PDFDetailView):
    template_name = ThreeDView.template_name
    model = ThreeDView.model
    context_object_name = 'score'
    version = '3d'
    cmd_options = {
        'orientation': 'Portrait',
    }


class SummaryView(DetailView):
    model = Score
    template_name = "analyzer/summary.html"
    context_object_name = 'score'


class SummaryPDFView(PDFDetailView):
    template_name = SummaryView.template_name
    model = SummaryView.model
    context_object_name = 'score'
    version = 'result'
    cmd_options = {
        'orientation': 'Portrait',
    }


class GuidelineView(TemplateView):
    template_name = "analyzer/guideline.html"


class GuidelinePDFView(PDFTemplateView):
    template_name = GuidelineView.template_name


class ScoreListDownload(ListView):
    def get(self, *args, **kwargs):
        grouped_scores = Score.objects.by_professional(
            self.request.user).group_by_survey().values()
        some_score = tuple(grouped_scores)[0][0]

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = (
            'attachment; filename="%s_%s.csv"' % (
                some_score.user,
                some_score.creation_date.strftime('%Y-%m-%d')
            )
        )

        # prepare CSV writer
        writer = csv.writer(response, delimiter=",")

        rows = []
        for scores in grouped_scores:
            # write headers
            serialized = ScoreCSVSerializer.serialize(scores[0])
            rows.append(serialized['header'])

            # write body
            for score in scores:
                serialized = ScoreCSVSerializer.serialize(score)
                rows.append([str(value) for value in serialized['body']])

        [writer.writerow(row) for row in rows]

        return response
