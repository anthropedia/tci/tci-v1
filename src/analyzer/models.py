#-*- coding: utf-8 -*-
import operator

from django.db.models.signals import post_save
from django.db.models.query import QuerySet
from django.db import models
from django.dispatch import receiver

from django_countries.fields import CountryField
from jsonfield import JSONField
from model_utils.managers import PassThroughManager

from account import constants as account_constants
from survey.models import UserResult, UserResultAnswer, Question, Trait, Norm
from . import utils
from .constants import TEMPERAMENT

from . import constants


class ScoreValidation(object):
    def __init__(self, data):
        self.data = data

    @property
    def validity(self):
        """
        returns the validity value. -1 is invalid, 0 is perhaps valid,
        1 is valid.
        """
        raise NotImplementedError("The `value` method should be overriden.")

    @property
    def is_valid(self):
        """
        True (1), False (2), or None (0) when perhaps valid.
        """
        return [None, True, False][self.validity]


class ScoreValidationRunLength(ScoreValidation):
    def longest_repetition_length(self, input_list=[]):
        """
        Retrieves the count of the longest consecutive identital values of
        a list.
        """
        best_element = None
        length = 0
        current = None
        current_length = 0
        for element in input_list:
            if current != element:
                current = element
                current_length = 1
            else:
                current_length = current_length + 1
            if current_length > length:
                best_element = current
                length = current_length
        return length

    @property
    def validity(self):
        length = self.longest_repetition_length(self.data)
        if length < 8:
            return 1
        elif length < 10:
            return 0
        else:
            return -1


class RawscoreCalculator(object):
    def __init__(self, result):
        self.result = result

    def subtraits_queryset(self):
        """
        Retrieves all the subtraits with their calculated rawscore and
        avg_rawscore.
        """
        return (self.result.answers
                .values('answer__question__trait__abbreviation')
                .exclude(answer__question__trait__abbreviation__isnull=True)
                .annotate(rawscore=models.Sum('answer__value'))
                .annotate(avg_rawscore=models.Avg('answer__value'))
                .values('answer__question__trait__abbreviation', 'rawscore',
                        'avg_rawscore')
                .order_by('answer__question__trait'))

    def maintraits_queryset(self):
        """
        Retrieves all the maintraits with their calculated rawscore and
        avg_rawscore.
        > Time + luck = headache + super query.
        """
        return (self.result.answers
                .order_by('answer__question__trait')
                .exclude(answer__question__trait__abbreviation__isnull=True)
                .values('answer__question__trait__abbreviation')
                .values('answer__question__trait__parent__abbreviation')
                .annotate(rawscore=models.Sum('answer__value'))
                .annotate(avg_rawscore=models.Avg('answer__value'))
                .values('answer__question__trait__parent__abbreviation',
                        'rawscore', 'avg_rawscore'))

    def maintraits(self):
        return dict(self.maintraits_queryset().values_list(
            'answer__question__trait__parent__abbreviation', 'rawscore'))

    def average_maintraits(self):
        avg_lists = {}
        for k, v in self.average_subtraits().items():
            abbr = k[:2]
            if abbr not in avg_lists.keys():
                avg_lists[abbr] = []
            avg_lists[abbr].append(v)
        return {k: (sum(vs) / len(vs)) for k, vs in avg_lists.items()}

    def subtraits(self):
        return dict(self.subtraits_queryset().values_list(
            'answer__question__trait__abbreviation', 'rawscore'))

    def average_subtraits(self):
        return dict(self.subtraits_queryset().values_list(
            'answer__question__trait__abbreviation', 'avg_rawscore'))

    def traits(self):
        ts = self.maintraits()
        ts.update(self.subtraits())
        return ts

    def averages(self):
        ts = self.average_maintraits()
        ts.update(self.average_subtraits())
        return ts

    def maintrait_percentages(self):
        """
        Percentage of a maintrait compared to the maximum possible value.
        """
        return {abbr: (score - 1) * 25
                for abbr, score in self.average_maintraits().items()}

    def subtrait_percentages(self):
        """
        Percentage of the rawscore for a subtrait compared to its maintrait.
        """
        # Sum all subtraits by trait
        main_rawscores = {}
        for abbr, score in self.average_subtraits().items():
            main_abbr = abbr[:2]
            if main_abbr not in main_rawscores.keys():
                main_rawscores[main_abbr] = 0
            main_rawscores[main_abbr] += score
        # calculate substrait % trait
        percentages = {}
        for abbr, rawscore in self.average_subtraits().items():
            percentages[abbr] = (
                float(rawscore) / float(main_rawscores[abbr[:2]])) * 100.0
        return percentages

    def trait_percentages(self):
        traits = self.maintrait_percentages()
        traits.update(self.subtrait_percentages())
        return traits

    def personality_maintraits(self):
        """linearized average rawscore.
        returns a dict of normalized contributions values."""
        return {k: utils.linearize_rawscore(k, v)
                for k, v in self.average_maintraits().items()}

    def personality_maintrait_percentages(self):
        """returns a dict of normalized contributions percentages
        compared to the total of the personality"""
        contributions = self.personality_maintraits()
        total = sum(contributions.values())
        return {k: (v / total) * 100 for k, v in contributions.items()}

    def personality_subtrait_percentages(self):
        """"""
        results = {}
        linear_averages = {k: utils.linearize_rawscore(k, v)
                           for k, v in self.averages().items()}
        linear_totals = {}
        linear_percentages = {}
        # calculate linear totals
        for k, v in linear_averages.items():
            if len(k) == 2:
                continue
            abbr = k[:2]
            if abbr not in linear_totals:
                linear_totals[abbr] = 0
            linear_totals[abbr] += v
        # calculate personality percentages
        for abbr, avg in self.averages().items():
            if len(abbr) == 2:
                continue
            linear_averages[abbr] = utils.linearize_rawscore(abbr, avg)
            linear_percentages[abbr] = linear_averages[abbr] / linear_totals[abbr[:2]]
            maintraits_personality = self.personality_maintrait_percentages()
            results[abbr] = linear_percentages[abbr] * maintraits_personality[abbr[:2]]
        return results

    def personality_percentages(self):
        """
        calculate the impact (%) of a (sub)trait with the personality.
        """
        percentages = self.personality_maintrait_percentages()
        percentages.update(self.personality_subtrait_percentages())
        return percentages

    def qualitatives(self):
        qualitatives = ['very_low', 'low', 'average_low', 'average_high',
                        'high', 'very_high']
        extractor = ScoreExtractor(self.result)
        rs_qualitatives = {}
        rawscore_averages = self.average_maintraits()
        for trait in extractor.get_maintraits():
            abbr = trait.abbreviation
            delta = 4.0 / len(qualitatives)
            score = rawscore_averages[abbr]
            index = int((score - 1) / delta) if score < 5 else -1
            rs_qualitatives[abbr] = qualitatives[index]
        return rs_qualitatives


class ScoreCalculator(object):
    """
    Processes data and calculates scores for a UserResult.
    basically methods here returns moslty numbers.
    """

    _rawscores = None

    def __init__(self, result):
        self.result = result

    @property
    def rawscores(self):
        """
        This is a reference to an instance of `RawscoreCalculator`.
        """
        if not self._rawscores:
            self._rawscores = RawscoreCalculator(self.result)
        return self._rawscores

    def zscores(self):
        extractor = ScoreExtractor(self.result)
        norms = extractor.get_norms()
        scores = {}
        for trait, rawscore in self.rawscores.traits().items():
            norm = norms.get(trait)
            mean = norm.mean_value
            dev = norm.standard_deviation
            score = (rawscore - mean) / dev if dev else 0
            scores[trait] = score
        return scores

    def tscores(self):
        return {trait: round(50 + zscore * 10, 2)
                for trait, zscore in self.zscores().items()}

    def percentiles(self):
        """
        XXX: Retriving the norms might be process consuming.
        """
        extractor = ScoreExtractor(self.result)
        percents = {}
        norms = extractor.get_norms()
        default_percentiles = Norm.objects.get_default().percentiles.all()
        tscores = self.tscores()
        rawscores = self.rawscores.traits()
        for trait in self.result.survey.traits:
            abbr = trait.abbreviation
            norm = norms[abbr]
            if norm.percentiles_count:
                score = rawscores[abbr]
                # We use `percentile_set` and not `psercentiles` here to avoid
                # double querying.
                percentile = norm.percentile_set.get_by_score(score)
            else:
                score = tscores[abbr]
                percentile = default_percentiles.get_by_score(score)
            percents[abbr] = percentile.value
        return percents


class ScoreExtractor(object):
    """
    Extracts values, keys and data out of a UserResult object.
    Does not process formula but filtered data.
    """
    def __init__(self, result, *args, **kwargs):
        self.result = result
        self.calculator = ScoreCalculator(result)

    def get_raw_answers(self):
        return [int(p + 1) for p in self.result.answers.all().values_list(
            'answer__position', flat=True)]

    def get_answer_values(self):
        """
        Returns the list of answers value ordered by question.
        """
        return [int(v) for v in (
            UserResultAnswer.objects
            .filter(result=self.result)
            .order_by('question__position')
            .values_list('answer__value', flat=True))]

    def get_validity_questions_indexes(self):
        """
        Returns the list if validity question positions (starts with 0).
        """
        return(Question.objects.filter(is_validity=True)
               .filter(survey=self.result.survey)
               .values_list('position', flat=True))

    def get_related_subtraits(self):
        """
        Returns a QuerySet of all subtraits (non-traits) associatied to the
        result.
        XXX: Move to Trait's object?
        """
        return Trait.objects.filter(pk__in=self.result.survey.questions
                                    .values_list('trait__pk'))

    def get_norms(self):
        """
        Retrieves a {<trait abbr>: <Norm>, …} object for the current `result`.
        XXX: time consuming! (process consuming maybe as well?)
        """
        norms = (Norm.objects.by_user_result(self.result)
                 .select_related('percentile_set')
                 .annotate(percentiles_count=models.Count('percentile_set')))
        return {n.trait.abbreviation: n for n in norms}

    def get_maintraits(self, construct=None):
        """
        Retrieves a list of Trait (non-subtraits) objects.
        If specified, construct will filter by Trait construct property.
        """
        qs = self.result.survey.maintraits
        if construct:
            qs = qs.filter(construct=construct)
        return qs

    def get_subtraits_to_maintraits(self):
        """
        Returns a dict or subtraits to their maintraits;
        {u'ps': u'ps4', u'ns': u'ns4', u'co': u'co3', u'st': u'st2', …}
        """
        if not hasattr(self, '_subtraits_to_maintraits'):
            qs = (self.result.survey.traits
                      .exclude(abbreviation=None)
                      .exclude(models.Q(children__abbreviation=None) |
                               models.Q(children__abbreviation=""))
                      .values_list('children__abbreviation', 'abbreviation'))
            setattr(self, '_subtraits_to_maintraits', dict(qs))
        return self._subtraits_to_maintraits

    def get_traits_tree(self):
        """
        Returns an object where keys are maintraits abbreviation
        and values are a list with their respective subtraits abbreviation.
        {u'co': [u'co4', u'co5', u'co1', u'co2', u'co3'],
         u'ha': [u'ha1', u'ha2', u'ha3', u'ha4'], …}
        """
        traits = self.get_subtraits_to_maintraits()
        tree = {}
        for key, value in traits.items():
            tree.setdefault(value, []).append(key)
        return tree


class ScoreGeneratorManager(models.Manager):
    def from_result(self, result):
        try:
            patient = result.user.patient_profile
        except AttributeError:
            patient = None
        extractor = ScoreExtractor(result)
        calculator = ScoreCalculator(result)
        raw_answers = list(extractor.get_raw_answers())
        converted_data = {
            'survey_name': str(result.survey.title),
            'name': str(result.user),
            'reference': getattr(patient, 'reference', None),
            'gender': getattr(patient, 'gender', None),
            'country': getattr(getattr(patient, 'country', None), 'code', None),
            'education_level': getattr(patient, 'education_level', None),
            'ethnicity': getattr(patient, 'ethnicity', None),
            'marital_status': getattr(patient, 'marital_status', None),
            'age': getattr(patient, 'age', None),
            'raw_answers': raw_answers,
            'answer_values': list(extractor.get_answer_values()),
            #'validity': {'longest_run': ScoreValidationRunLength(raw_answers),
            #    'questions': VALIDITY_QUESTIONS_VALUES_TODO},
            'validity_questions': list(
                extractor.get_validity_questions_indexes()),
            'rawscores': dict(calculator.rawscores.traits()),
            'rawscore_averages': dict(calculator.rawscores.averages()),
            'rawscore_qualitatives': dict(calculator.rawscores.qualitatives()),
            'rawscore_percentages': dict(
                calculator.rawscores.trait_percentages()),
            'personality_percentages': dict(
                calculator.rawscores.personality_percentages()),
            #'zscores': dict(calculator.zscores()),
            'tscores': dict(calculator.tscores()),
            'percentiles': dict(calculator.percentiles()),
        }
        (score, created) = self.update_or_create(result=result,
                                                 defaults=converted_data)
        return score


class ScoreQuerySet(QuerySet):
    use_for_related_fields = True

    def by_professional(self, professional):
        return self.filter(
            result__user__patient_profile__professional=professional)

    def group_by_survey(self):
        surveys = {}
        for score in self:
            survey_title = score.survey_name
            if not survey_title in surveys.keys():
                surveys[survey_title] = []
            surveys[survey_title].append(score)
        return surveys


class Score(models.Model):
    """
    This model is a freeze of a Survey score.
    It was created to cache a survey result and its calculations for optimizing
    display time and ease of searching for statisticians.
    """
    #--- original reference
    # the source result used to generate this score
    result = models.OneToOneField(UserResult, related_name='score')
    # the survey title
    survey_name = models.CharField(max_length=100)

    #--- patient's data
    name = models.CharField(max_length=100, null=True)
    reference = models.CharField(max_length=100, null=True)
    gender = models.CharField(max_length=1, null=True,
                              choices=account_constants.GENDER)
    country = CountryField(null=True)
    education_level = models.CharField(max_length=20, null=True,
                                       choices=account_constants.EDUCATION)
    ethnicity = models.CharField(max_length=20, null=True,
                                 choices=account_constants.ETHNICITY)
    marital_status = models.CharField(max_length=20, null=True,
                                      choices=account_constants.MARITAL)
    age = models.PositiveSmallIntegerField(null=True)

    #--- cached data
    # list of which answers were checked,
    # ordered by question. ex: [3, 4, 4, ...]
    # This is useful for validity run length
    raw_answers = JSONField()
    #--- cached data
    # list of answer values ordered by question. ex: [3, 4, 2, ...]
    answer_values = JSONField()
    # The validity question indexes. ex: [3, 54, 67, 121, 232]
    validity_questions = JSONField()
    #  classified by (sub)traits. ex {'ns': 3.3, 'ns2': 2.4, ...}
    rawscores = JSONField()
    #  classified by (sub)traits. ex {'ns': 3.12, 'ns2': 2.4445, ...}
    rawscore_averages = JSONField()
    # percentage of the average rawscores by subtraits (no maintrait).
    # ex: {'ns': 55.2, 'ns2': 24.8, ...}
    rawscore_percentages = JSONField()
    # ex: {'ns': 18, 'ns2': 4, ...}
    personality_percentages = JSONField()
    # average raw scores by maintraits. ex: {'ns': "low", 'ha': "average_high"}
    rawscore_qualitatives = JSONField()
    # zscores by subtraits. ex: {'ns': 3.3, 'ns2': 2.4, ...}
    zscores = JSONField()
    # tscores by (sub)traits. ex: {'ns': 56, 'ns2': 12, ...}
    tscores = JSONField()
    # percentiles by (sub)traits. ex: {'ns': 40, 'ns2': 78, ...}
    percentiles = JSONField()
    # extra data that could be needed for some specific model.
    extra = JSONField()
    creation_date = models.DateField(auto_now_add=True)

    objects = PassThroughManager.for_queryset_class(ScoreQuerySet)()
    generator = ScoreGeneratorManager()

    @property
    def validity(self):
        answers = [self.answer_values[i] for i in self.validity_questions]
        return (sum(answers), len(answers))

    # Alias
    @property
    def survey(self):
        return self.result.survey

    # Alias
    @property
    def user(self):
        return self.result.user

    # Alias
    @property
    def display_subtraits(self):
        return self.result.survey.display_subtraits

    @property
    def lowest_subtraits(self):
        scores = self.rawscore_averages
        return [t for t in sorted(scores.items(), key=operator.itemgetter(1))
                if len(t[0]) == 3]

    @property
    def highest_subtraits(self):
        highests = self.lowest_subtraits
        highests.reverse()
        return highests

    def is_subtrait_high(self, abbr, count=3):
        return abbr in dict(self.highest_subtraits[:count]).keys()

    def is_subtrait_low(self, abbr, count=3):
        return abbr in dict(self.lowest_subtraits[:count]).keys()

    def is_maintrait_high(self, abbr):
        return 'high' in self.rawscore_qualitatives[abbr]

    @property
    def temperament_type(self):
        (rd, ns, ha) = [self.is_maintrait_high(t) for t in ['rd', 'ns', 'ha']]
        return constants.TEMPERAMENT_TYPES[rd][ns][ha]

    @property
    def character_type(self):
        (co, st, sd) = [self.is_maintrait_high(t) for t in ['co', 'st', 'sd']]
        return constants.CHARACTER_TYPES[co][st][sd]

    @property
    def extractor(self):
        if not hasattr(self, '_extractor'):
            _extractor = ScoreExtractor(self.result)
            setattr(self, '_extractor', _extractor)
        return self._extractor

    def __str__(self):
        return str(self.result)

    class utils:
        @staticmethod
        def is_maintrait(abbr):
            return len(abbr) == 2


@receiver(post_save, sender=UserResult)
def generate_scores(sender, instance, **kwargs):
    if instance.is_complete:
        try:
            instance.score
        except Score.DoesNotExist:
            Score.generator.from_result(instance)
