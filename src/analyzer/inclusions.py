from jingo import register
from django.db.models import Q

from default import _

from . import constants
from .models import Score


tp = "analyzer/inclusions/%s.html"


def _calculation_2d(score, trait_x, trait_y):
    suffix_x = 'h' if score.is_maintrait_high(trait_x) else 'l'
    suffix_y = 'h' if score.is_maintrait_high(trait_y) else 'l'
    trait_x_abbr = '%s-%s' % (trait_x, suffix_x)
    trait_y_abbr = '%s-%s' % (trait_y, suffix_y)
    value_x = score.rawscore_percentages[trait_x]
    value_y = score.rawscore_percentages[trait_y]
    return locals()


@register.function
def result_2d(view, trait_x, trait_y, suffix=''):
    ctx = _calculation_2d(view.object, trait_x, trait_y)
    res = "results.2d.%s-%s_%s-%s" % (trait_x, ctx['suffix_x'],
                                      trait_y, ctx['suffix_y'])
    if suffix:
        res = "%s.%s" % (res, suffix)
    return _(res)


@register.inclusion_tag(template=tp % 'graph_1d')
def graph_1d(score, construct=None, with_titles=True):
    if not isinstance(score, Score):  # score could be a View instance
        score = score.score
    return {
        'score': score,
        'construct': construct,
        'with_titles': with_titles,
    }


@register.inclusion_tag(template=tp % 'graph_1d_legend')
def graph_1d_legend():
    return {}


@register.inclusion_tag(template=tp % 'graph_2d')
def graph_2d(view, trait_x, trait_y, details=True):
    context = _calculation_2d(view.object, trait_x, trait_y)
    context.update(locals())
    return context


@register.inclusion_tag(template=tp % 'graph_3d')
def graph_3d(view, type, details=True):
    labels = getattr(constants, '%s_TYPES' % type.upper())
    result = getattr(view.object, '%s_type' % type)
    reversed = type in ('temperament',)
    return locals()


@register.inclusion_tag(template=tp % 'result_validity')
def result_validity(score):
    return {'score': score.validity[0]}


@register.inclusion_tag(template=tp % 'result_traits_table')
def result_traits_table(score, *args, **kwargs):
    traits = score.survey.traits
    count = kwargs.get('count', 3)
    if len(args):
        traits = traits.filter(Q(abbreviation__in=args) |
                               Q(parent__abbreviation__in=args))
    return locals()


@register.inclusion_tag(template=tp % 'result_integration')
def result_integration(score, abbr):
    subtraits = score.extractor.get_traits_tree()[abbr]
    scores = sorted([score.rawscore_averages[t] for t in subtraits])
    # 4 is the maximum possible diff value and we want to reverse the score.
    #value = 4 - (scores[-1] - scores[0])
    value = scores[-1] - scores[0]
    return {'value': value}


@register.function
def result_trait(score, trait, suffix=''):
    high = 'high' in score.rawscore_qualitatives[trait]
    res = "results.1d.%s.%s" % (trait, 'high' if high else 'low')
    if suffix:
        res = "%s.%s" % (res, suffix)
    return _(res)


@register.inclusion_tag(template=tp % 'result_trait_qualitative')
def result_trait_qualitative(score, abbr):
    return {
        'score': score.rawscore_qualitatives[abbr]
    }


@register.inclusion_tag(template=tp % 'result_subtraits_table')
def result_lowest_subtraits(score, count=3):
    return {
        'score': score,
        'traits': score.lowest_subtraits[:count],
    }


@register.inclusion_tag(template=tp % 'result_subtraits_table')
def result_highest_subtraits(score, count=3):
    return {
        'score': score,
        'traits': score.highest_subtraits[:count],
    }
