from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.decorators import permission_required
from django.contrib import messages
from django.views.generic import FormView, UpdateView

from default.decorators import view_decorator

from .models import UserResult
from .forms import AssignPatientForm, SurveyRunForm
from default import _


@view_decorator(permission_required('survey.run'))
class SurveyRunView(UpdateView):
    model = UserResult
    form_class = SurveyRunForm
    template_name = 'survey/survey_form.html'
    success_url = reverse_lazy('account:home')

    def form_valid(self, form):
        redirect = super(SurveyRunView, self).form_valid(form)
        if form.instance.is_complete:
            messages.success(self.request,
                             _("message.survey.complete"))
        else:
            messages.warning(self.request, _("message.survey.incomplete"))
        return redirect


@view_decorator(permission_required('professional.assign_patient'))
class AssignPatientView(FormView):
    model = UserResult
    template_name = "survey/assign_patient_form.html"
    form_class = AssignPatientForm
    success_url = reverse_lazy('account:home')

    def get_form_kwargs(self, **kwargs):
        kwargs = super(AssignPatientView, self).get_form_kwargs(**kwargs)
        kwargs['extra'] = {
            'professional': self.request.user
        }
        return kwargs

    def form_valid(self, form):
        if not form.save():
            messages.error(self.request,
                           _("Something went wrong when trying to assign "
                             "a user. Your credits were not debited."))
        return super(AssignPatientView, self).form_valid(form)
