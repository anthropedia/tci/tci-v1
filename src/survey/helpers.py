from jingo import register

from .models import Trait


@register.filter
def trait(value):
    try:
        return Trait.objects.get(abbreviation=value)
    except (Trait.DoesNotExist, Trait.MultipleObjectsReturned):
        return None
