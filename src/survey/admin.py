from django.contrib import admin

from django_mptt_admin.admin import DjangoMpttAdmin

from default import forms
from shop.models import SurveyPrice

from .models import (Survey, SurveyResult, Question, Answer, Trait, Norm,
                     Percentile)
from .forms import TraitForm


class QuestionForm(forms.ModelForm):

    class Meta:
        exclude = ('position',)


class QuestionInlineForm(QuestionForm):
    class Meta:
        exclude = ()


class QuestionInline(admin.TabularInline):
    sortable_field_name = 'position'
    model = Question
    extra = 0
    form = QuestionInlineForm


class SurveyResultInline(admin.TabularInline):
    model = SurveyResult
    extra = 0


class SurveyPriceInline(admin.TabularInline):
    model = SurveyPrice
    extra = 0


class SurveyAdmin(admin.ModelAdmin):
    inlines = [QuestionInline, SurveyResultInline, SurveyPriceInline]

admin.site.register(Survey, SurveyAdmin)


class AnswerInline(admin.TabularInline):
    model = Answer
    sortable_field_name = 'position'
    extra = 0


class QuestionAdmin(admin.ModelAdmin):
    inlines = [AnswerInline]
    list_filter = ('survey',)
    search_fields = ('title',)
    list_display = ('title', 'is_validity', 'survey', 'answers_count')
    change_list_filter_template = "admin/filter_listing.html"
    change_list_template = "admin/change_list_filter_sidebar.html"
    form = QuestionForm

admin.site.register(Question, QuestionAdmin)


class TraitAdmin(DjangoMpttAdmin):
    form = TraitForm

admin.site.register(Trait, TraitAdmin)


class PercentileInline(admin.TabularInline):
    model = Percentile
    extra = 0


class NormAdmin(admin.ModelAdmin):
    inlines = [PercentileInline]

admin.site.register(Norm, NormAdmin)
