from django.core.exceptions import ValidationError

from account.models import PatientProfile
from default import forms
from default import _
from shop.models import SurveyCredit

from .models import Survey, UserResult, UserResultAnswer, Trait


class SurveyRunForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        """
        This script builds the questions/answers for a survey and re-populates
        the POST data.
        """
        instance = kwargs.get('instance')
        super(SurveyRunForm, self).__init__(*args, **kwargs)
        self.build_questions(instance.survey,
                             dict(instance.answers.all()
                             .values_list('question_id', 'answer_id')))

    def build_questions(self, survey, data={}):
        """
        Generates question fields in self from survey.questions
        """
        for question in survey.questions.all():
            choices = [(a.pk, _(a.title, 'survey_test'))
                       for a in question.answers.all()]
            self.fields[question.pk] = forms.ChoiceField(
                label=question.title, choices=choices,
                widget=forms.RadioSelect, required=False,
                initial=data.get(question.pk))

    def is_valid(self):
        """
        form is always valid
        """
        return True

    def save(self, *args, **kwargs):
        kwargs['commit'] = False
        instance = super(SurveyRunForm, self).save(*args, **kwargs)
        for k, v in self.data.items():
            try:
                UserResultAnswer.objects.get_or_create(result=instance,
                                                       question_id=int(k),
                                                       defaults={'answer_id':
                                                                 int(v)})
            except ValueError:  # not a answer data (csrf?)
                pass
        instance.save()
        return instance

    class Meta:
        model = Survey
        fields = []


class AssignPatientForm(forms.Form):
    survey = forms.ModelChoiceField(queryset=Survey.objects)
    patients = forms.ModelMultipleChoiceField(queryset=PatientProfile.objects)

    def __init__(self, *args, **kwargs):
        super(AssignPatientForm, self).__init__(*args, **kwargs)
        self.fields['patients'].queryset = (
            self.extra['professional'].profile.patients)
        self.fields['survey'].queryset = (
            self.fields['survey'].queryset
            .for_profile(self.extra['professional'].profile))
        patients_widget = self.fields['patients'].widget
        self.fields['patients'].widget = forms.CheckboxSelectMultiple(
            choices=patients_widget.choices
        )

    def clean_patients(self):
        """
        Check that the number of patient is lower that the credits remaining.
        """
        patients = self.cleaned_data.get('patients', [])
        survey = self.cleaned_data.get('survey', None)
        if not survey:
            return None
        credit, created = SurveyCredit.objects.get_or_create(
            user=self.extra['professional'], survey=survey)
        if credit.amount < patients.count():
            raise ValidationError(
                _("assign.form.error.credit (available, total)").format(
                    available=credit.amount, total=patients.count()))
        return patients

    def save(self, *args, **kwargs):
        survey = self.cleaned_data.get('survey')
        professional = self.extra['professional']
        credits_count = self.cleaned_data.get('patients').count()
        decreased = False
        try:
            SurveyCredit.objects.decrease(
                professional, survey, credits_count)
            decreased = True
            for p in self.cleaned_data['patients']:
                UserResult.objects.create(user=p.user, survey=survey)
            return True
        except Exception as e:
            # if we had an error after removing credit, we should return it.
            if decreased:
                SurveyCredit.objects.increase(
                    professional, survey, credits_count)
            raise e
            return False


class TraitForm(forms.ModelForm):
    def clean(self):
        d = self.cleaned_data
        # subtrait should not have 'construct'
        if d['parent']:
            d['construct'] = None
        # maintrait must have a 'construct' value
        elif not d['construct']:
            raise ValidationError(
                _("The construct field is required for main traits"))
        return d

    class Meta:
        model = Trait
        exclude = []
