import datetime

from django.db import models
from django.conf import settings
from django.db.models import Q
from django.dispatch import receiver
from django.db.models.query import QuerySet

from django_countries.fields import CountryField

from mptt.models import MPTTModel, TreeForeignKey, TreeManager

from model_utils import Choices
from model_utils.models import StatusModel
from model_utils.managers import PassThroughManager

from default.utils import  model_slugifier
from . import constants


class SurveyQuerySet(QuerySet):
    use_for_related_fields = True

    def for_profile(self, profile=None):
        if not profile:
            return self
        profile_name = profile.profile_type
        types = {'coach': 1, 'clinician': 2, 'researcher': 3}
        return self.filter(
            models.Q(user_type__contains=types.get(profile_name)) |
            models.Q(user_type=None)
        )


class Survey(models.Model):
    """
    A concrete survey with questions
    """
    USER_TYPES = Choices(  # ('123', "All"),
        ('', "all"),
        ('1', "Coach"),
        ('2', "Clinician"),
        ('3', "Researcher"),
        ('23', "Clinician and researcher"),
    )
    title = models.CharField(max_length=255)
    user_type = models.CharField(max_length=255, choices=USER_TYPES,
                                 default="",
                                 null=True, blank=True,
                                 help_text="Who can purchase this survey?")
    display_subtraits = models.BooleanField(default=True)
    slug = models.SlugField()

    objects = PassThroughManager.for_queryset_class(SurveyQuerySet)()

    def __str__(self):
        return str(self.title)

    @property
    def calculation_type(self):
        """
        return "basic" for simple Survey calculation,
        "advanced" if trait calculation if required.
        """
        if self.subtraits.count() > 0:
            return 'advanced'
        else:
            return 'basic'

    @property
    def subtraits(self):
        """
        Returns a cached QuerySet with all subtraits for all questions.
        Note that there can be multiple times the same subtrait.
        """
        return self.traits.filter(level__gt=0)

    @property
    def maintraits(self):
        """
        Retrieves the list of main Traits objects related to this UserResult.
        """
        return self.traits.filter(level=0)

    @property
    def traits(self):
        """
        Return a `QuerySet` containing all the subtraits and maintraits related
        to this `Survey`.
        """
        if not getattr(self, '_cached_traits', None):
            setattr(self, '_cached_traits', Trait.objects.filter(
                    Q(children__questions__survey=self)
                    | Q(questions__survey=self))
                    .distinct())
        return self._cached_traits

    def calculate_price(self, quantity, currency='usd'):
        survey_price = (self.surveyprice_set.get_for_quantity(
            quantity=quantity, currency=currency))
        if survey_price:
            return survey_price.price * quantity

    def get_subtrait(self, abbreviation):
        return self.subtraits.get(abbreviation=abbreviation)


models.signals.pre_save.connect(model_slugifier, sender=Survey)


class QuestionQuerySet(QuerySet):
    use_for_related_fields = True

    def validity(self):
        return self.filter(is_validity=True)

    def regular(self):
        return self.exclude(is_validity=True)


class Question(models.Model):
    survey = models.ForeignKey(Survey, related_name='questions')
    title = models.CharField(max_length=255)
    trait = models.ForeignKey('Trait', null=True, on_delete=models.SET_NULL,
                              related_name='questions')
    position = models.PositiveIntegerField()
    is_validity = models.BooleanField(default=False)

    objects = PassThroughManager.for_queryset_class(QuestionQuerySet)()

    def __str__(self):
        return str(self.title)

    @property
    def answers_count(self):
        return self.answers.count()

    def save(self, *args, **kwargs):
        # if not self.position:
        #    previous = Question.objects.order_by('-position')[0].position
        #    self.position = previous + 1
        return super(Question, self).save(*args, **kwargs)

    class Meta:
        ordering = ['position']


class AnswerManager(models.Manager):
    use_for_related_fields = True

    def validity(self):
        return self.filter(question__is_validity=True)


class Answer(models.Model):
    question = models.ForeignKey(Question, related_name='answers')
    title = models.CharField(max_length=255)
    value = models.IntegerField()
    position = models.PositiveIntegerField(null=True)

    objects = AnswerManager()

    def __str__(self):
        return str(self.title)

    class Meta:
        ordering = ['position']


class TraitManager(TreeManager):
    """
    As this MUST be a TreeManager we cannot set it as QuerySet.
    A QuerySet here fails reordering items.
    """

    use_for_related_fields = True

    def by_construct(self, construct):
        return self.filter(construct=construct)


class Trait(MPTTModel):

    # Blank is here because we need an empty option in admin,
    # field.required = False does not the job.
    parent = TreeForeignKey('self', null=True, blank=True,
                            related_name='children')
    title = models.CharField(max_length=50, unique=True)
    abbreviation = models.CharField(max_length=3)
    construct = models.CharField(max_length=20, choices=constants.CONSTRUCT,
                                 null=True)
    label = models.CharField(max_length=100, null=True)

    tree = TreeManager()
    objects = TraitManager()

    def __str__(self):
        return self.label or "%s (%s)" % (self.abbreviation, self.title)

    @property
    def is_maintrait(self):
        return self.level == 0

    @property
    def is_subtrait(self):
        return self.level == 1

    @property
    def type(self):
        return 'maintrait' if self.is_maintrait else 'subtrait'

    @property
    def maintrait(self):
        """
        return the parent Trait if current item is a subtrait, itself otherwise
        """
        if self.is_maintrait:
            return self
        else:
            return self.parent

    @property
    def subtraits(self):
        if not self.is_maintrait:
            return None
        return self.get_children()

    @property
    def siblings(self):
        return self.parent.get_children()

    @property
    def percentage(self):
        """
        Calculates the percentage that the Trait represents compared to its
        siblings.
        """
        return round(100 / self.siblings.count(), 2)

    @classmethod
    def subtrait_to_maintrait(klass):
        """
        Tuple
        """
        return Trait.objects.all().values_list('abbreviation',
                                               'parent__abbreviation')

    class MPTTMeta:
        order_insertion_by = ['abbreviation']


@receiver(models.signals.pre_save, sender=Trait)
def trait_lower_abbreviation(sender, instance, **kwargs):
    instance.abbreviation = instance.abbreviation.lower()


# Disable as it breaks the ability to administate ordering
#@receiver(models.signals.post_save, sender=Trait)
# def trait_fix_tree(sender, instance, **kwargs):
#    Trait.objects.rebuild()


class NormQuerySet(QuerySet):

    DEFAULT_COUNTRY = 'FR'

    def regular(self):
        return self.exclude(trait__abbreviation="00")

    def get_default(self):
        return self.get(trait__abbreviation="00")

    def country(self, country):
        if isinstance(country, str):
            country = country.upper()
        return self.regular().filter(Q(country=country) |
                                     Q(country=self.DEFAULT_COUNTRY))

    def countries(self):
        # return Norm.objects.all().distinct('country')  ## Not on Sqlite
        return list(set(Norm.objects.all()
                            .values_list('country', flat=True)))

    def get_by_natural_key(self, country, trait):
        return self.get(country=country.upper(), trait__abbreviation=trait)

    def by_user_result(self, user_result):
        return self.filter(
            Q(trait__questions__survey__result_set=user_result) |
            Q(trait__children__questions__survey__result_set=user_result),
            Q(country=user_result.user.patient_profile.survey_country) |
            Q(country='FR') |
            Q(country='US')  # Some norms in the DB are still defaulted in US.
            #  It breaks a TCI140 for example. I'm not changing them as the
            # impact is unknown and is time cunsuming to update.
        )


class Norm(models.Model):

    """
    Mean value with standard dev for a trait in a country
    """
    trait = models.ForeignKey(Trait)
    country = CountryField(null=True)
    mean_value = models.DecimalField(max_digits=5, decimal_places=2)
    standard_deviation = models.DecimalField(max_digits=5, decimal_places=2)

    objects = PassThroughManager.for_queryset_class(NormQuerySet)()

    def __str__(self):
        return "%s (%s)" % (self.trait, self.country)

    def natural_key(self):
        return (self.country.code, self.trait.abbreviation)

    @property
    def is_default(self):
        return self.trait.abbreviation == "00"

    @property
    def percentiles(self):
        values = self.percentile_set
        if not values.count():
            values = Norm.objects.get_default().percentile_set
        return values

    class Meta:
        ordering = ('trait__abbreviation', 'country')


class PercentileValueQuerySet(QuerySet):

    def get_by_score(self, score):
        percentile = (self.order_by('-raw_score_min')
                      .filter(raw_score_min__lte=score).first())
        # Not value is below, return the very minimal value
        if not percentile:
            percentile = self.order_by('-raw_score_min').first()
        return percentile


class Percentile(models.Model):
    norm = models.ForeignKey(Norm, related_name='percentile_set')
    raw_score_min = models.IntegerField(null=False)
    value = models.IntegerField(null=False)

    objects = PassThroughManager.for_queryset_class(PercentileValueQuerySet)()

    def __str__(self):
        return "%s %d: %d" % (self.norm, self.raw_score_min, self.value)


class SurveyResultQuerySet(QuerySet):

    def get_by_score(self, result):
        return (self.order_by('-min_value').get(min_value__lte=result.score)
                .limit(1))


class SurveyResult(models.Model):

    """
    Store survey display values for non "basic" surveys type.
    """
    survey = models.ForeignKey(Survey)
    min_value = models.PositiveIntegerField()
    max_value = models.PositiveIntegerField()
    title = models.CharField(max_length=100)
    body = models.TextField()

    objects = PassThroughManager.for_queryset_class(SurveyResultQuerySet)()

    def __str__(self):
        return str(self.title)

    class Meta:
        ordering = ['min_value']

    def traits(self):
        return self.maintraits() | self.subtraits()


class UserResultQuerySet(QuerySet):
    use_for_related_fields = True

    def incomplete(self):
        return self.exclude(status='complete')

    def complete(self):
        return self.filter(status='complete')

    def pending(self):
        return self.filter(status='pending')

    def progress(self):
        return self.filter(status='progress')

    def by_professional(self, professional):
        return self.filter(user__patient_profile__professional=professional)

    def group_by_survey(self):
        surveys = {}
        for result in self:
            survey_title = result.survey.title
            if not survey_title in surveys.keys():
                surveys[survey_title] = []
            surveys[survey_title].append(result)
        return surveys


class UserResult(StatusModel):
    """
    Many Users participate to many Surveys and have result for each.
    """
    STATUS = Choices('pending', 'progress', 'complete')
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             related_name='result_set')
    survey = models.ForeignKey(Survey, related_name='result_set')
    creation_date = models.DateTimeField(auto_now_add=True)
    modification_date = models.DateTimeField(auto_now=True)

    objects = PassThroughManager.for_queryset_class(UserResultQuerySet)()

    def __str__(self):
        return "%s - %s" % (self.user, self.survey)

    @property
    def related_traits(self):
        """
        Return a list of related traits
        """
        return Trait.objects.filter(pk__in=self.result.survey.questions)

    @property
    def result(self):
        """
        Result for `basic` type of survey.
        XXX: Still valid after the analyzer?
        """
        score = self.score
        try:
            return SurveyResult.objects.get(min_value__lte=score,
                                            max_value__gte=score)
        except SurveyResult.DoesNotExist:
            return None

    @property
    def is_complete(self):
        return self.status == 'complete'

    @property
    def missing_questions(self):
        return (self.survey.questions.exclude(pk__in=self.answers.all()
                                              .values_list('question')))

    class Meta:
        ordering = ['-modification_date', '-creation_date', 'user']


@receiver(models.signals.pre_save, sender=UserResult)
def user_result_set_status(sender, instance, **kwargs):
        count = instance.answers.count()
        if count == 0:
            status = 'pending'
        elif count < instance.survey.questions.count():
            status = 'progress'
        else:
            status = 'complete'
        instance.status = status


# this should happen, but creation_date happened not to exist on the server!!
@receiver(models.signals.pre_save, sender=UserResult)
def user_result_set_creation_date(sender, instance, **kwargs):
        if not instance.creation_date:
            instance.creation_date = datetime.date.today()


class UserResultAnswerQuerySet(QuerySet):

    def validity(self):
        return self.filter(question__is_validity=True)

    def regular(self):
        return self.exclude(question__is_validity=True)


class UserResultAnswer(models.Model):

    """
    Patient's answers for a program
    """
    question = models.ForeignKey(Question)
    answer = models.ForeignKey(Answer, related_name='user_answers')
    result = models.ForeignKey(UserResult, related_name='answers')

    objects = PassThroughManager.for_queryset_class(UserResultAnswerQuerySet)()

    def __str__(self):
        return "%s: %s" % (self.question, self.answer)

    class Meta:
        unique_together = ('result', 'question')


@receiver(models.signals.post_save, sender=UserResultAnswer)
def user_result_answer_post_save(sender, instance, **kwargs):
    instance.result.save()
