from django.test import TestCase

from survey.models import Survey

class SurveyModelTest(TestCase):

    def test_slug(self):
        survey = Survey.objects.create(title="TCI R v3")
        self.assertEqual(survey.slug, "tci-r-v3")
