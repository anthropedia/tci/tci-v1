# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import mptt.fields
import django.db.models.deletion
import django_countries.fields
import django.utils.timezone
from django.conf import settings
import model_utils.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Answer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('value', models.IntegerField()),
                ('position', models.PositiveIntegerField(null=True)),
            ],
            options={
                'ordering': ['position'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Norm',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('country', django_countries.fields.CountryField(max_length=2, null=True)),
                ('mean_value', models.DecimalField(max_digits=5, decimal_places=2)),
                ('standard_deviation', models.DecimalField(max_digits=5, decimal_places=2)),
            ],
            options={
                'ordering': ('trait__abbreviation', 'country'),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Percentile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('raw_score_min', models.IntegerField()),
                ('value', models.IntegerField()),
                ('norm', models.ForeignKey(related_name='percentile_set', to='survey.Norm')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('position', models.PositiveIntegerField()),
                ('is_validity', models.BooleanField(default=False)),
            ],
            options={
                'ordering': ['position'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Survey',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('user_type', models.CharField(default=b'', choices=[(b'', b'all'), (b'1', b'Coach'), (b'2', b'Clinician'), (b'3', b'Researcher'), (b'23', b'Clinician and researcher')], max_length=255, blank=True, help_text=b'Who can purchase this survey?', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SurveyResult',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('min_value', models.PositiveIntegerField()),
                ('max_value', models.PositiveIntegerField()),
                ('title', models.CharField(max_length=100)),
                ('body', models.TextField()),
                ('survey', models.ForeignKey(to='survey.Survey')),
            ],
            options={
                'ordering': ['min_value'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Trait',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(unique=True, max_length=50)),
                ('abbreviation', models.CharField(max_length=3)),
                ('construct', models.CharField(max_length=20, null=True, choices=[(b'character', b'Character'), (b'temperament', b'Temperament')])),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('parent', mptt.fields.TreeForeignKey(related_name='children', blank=True, to='survey.Trait', null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserResult',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', model_utils.fields.StatusField(default=b'pending', max_length=100, verbose_name='status', no_check_for_status=True, choices=[(b'pending', b'pending'), (b'progress', b'progress'), (b'complete', b'complete')])),
                ('status_changed', model_utils.fields.MonitorField(default=django.utils.timezone.now, verbose_name='status changed', monitor='status')),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('modification_date', models.DateTimeField(auto_now=True)),
                ('survey', models.ForeignKey(related_name='result_set', to='survey.Survey')),
                ('user', models.ForeignKey(related_name='result_set', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-modification_date', '-creation_date', 'user'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserResultAnswer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('answer', models.ForeignKey(related_name='user_answers', to='survey.Answer')),
                ('question', models.ForeignKey(to='survey.Question')),
                ('result', models.ForeignKey(related_name='answers', to='survey.UserResult')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='userresultanswer',
            unique_together=set([('result', 'question')]),
        ),
        migrations.AddField(
            model_name='question',
            name='survey',
            field=models.ForeignKey(related_name='questions', to='survey.Survey'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='question',
            name='trait',
            field=models.ForeignKey(related_name='questions', on_delete=django.db.models.deletion.SET_NULL, to='survey.Trait', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='norm',
            name='trait',
            field=models.ForeignKey(to='survey.Trait'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='answer',
            name='question',
            field=models.ForeignKey(related_name='answers', to='survey.Question'),
            preserve_default=True,
        ),
    ]
