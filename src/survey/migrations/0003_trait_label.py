# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

from survey.models import Trait


def restore_traits_names(apps, schema_editor):
    def replace(old, new, suffix):
        trait = None
        for t in Trait.objects.filter(abbreviation__startswith=old):
            if len(t.abbreviation) < 3:
                continue
            if not trait:
                trait = t
            t.abbreviation = '{}{}'.format(new, t.abbreviation[2])
            t.label = '{} - {}'.format(t.abbreviation, suffix)
            t.save()
        p = trait.parent if trait else None
        if p:
            p.label = '{} - {}'.format(new, suffix)
            p.save()
    for old, new in [('nv', 'ns'), ('hm', 'ha'), ('rw', 'rd'), ('pr', 'ps'),
                     ('dr', 'sd'), ('cp', 'co'), ('tr', 'st')]:
        replace(old, new, 'TCI140')
    replace('se', 'st', 'TCIR240')


class Migration(migrations.Migration):

    dependencies = [
        ('survey', '0002_survey_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='trait',
            name='label',
            field=models.CharField(max_length=100, null=True),
            preserve_default=True,
        ),
        migrations.RunPython(restore_traits_names),
    ]
