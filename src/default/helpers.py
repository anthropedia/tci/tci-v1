# -*- coding: utf-8 -*-

# Jinja2 helpers
import jinja2

from math import ceil, floor

from jingo import register

from django.utils.translation import get_language
from django.conf import settings as dj_settings


register.function(get_language)
register.function(ceil)
register.function(floor)
register.function(int)
register.function(abs)
register.function(len)


@register.function
@jinja2.contextfunction
def user_messages(context):
    return context['messages']


_round = round
@register.function
def round(value, count=0):
    return _round(float(value), count)

@register.filter
def prepend(value, string):
    return "%s%s" % (string, value)


@register.filter
def append(value, string):
    return "%s%s" % (value, string)


@register.function
def currencyfmt(value, currency):
    """
    formats a value placing the currency.
    """
    if currency.lower() == 'usd':
        display = "{symbol}{value}"
    else:
        display = "{value}{symbol}"

    symbols = {
        'usd': "$",
        'eur': "€",
    }
    symbol = symbols.get(currency.lower(), None)
    return display.format(**locals())


@register.function
def abs_static_url():
    """
    returns the abs_static_url from the settings.
    """
    return dj_settings.ABS_STATIC_URL


@register.function
def abs_url(url):
    return "%s%s" % (dj_settings.ABS_URL, url)


@register.function
def settings(key):
    """
    makes project settings available to templates.
    """
    return getattr(dj_settings, key.upper(), None)


@register.function
def debugger(*args):
    def p(o):
        print(">>> Template Helper debugger: ", args)
    try:
        p(args)
        import ipdb
        ipdb.set_trace()
    except ImportError:
        p(args)


@register.function
def current_url(view):
    return view.request.META.get('PATH_INFO', None) or "/"
