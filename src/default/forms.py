from copy import deepcopy

from django.forms import *
from django.db.models.fields import NOT_PROVIDED


class Form(Form):
    """
    Adds an optional `extra` kwargs when instantiating a `Form` to hold
    extra data.
    """
    def __init__(self, *args, **kwargs):
        self.extra = kwargs.pop('extra') if 'extra' in kwargs else {}
        super(Form, self).__init__(*args, **kwargs)


class ModelForm(ModelForm):
    """
    Set admin fields to required=False where a model field is null=True or
    it has a default value.
    This avoids writting blank=True.
    It also adds an optional `extra` kwargs to hold extra data.
    """
    def __init__(self, *args, **kwargs):
        self.extra = kwargs.pop('extra') if 'extra' in kwargs else {}
        super(ModelForm, self).__init__(*args, **kwargs)
        if not self._meta.model:
            raise AssertionError("A ModelForm must have a Meta with a model.")
        force_required = getattr(self.Meta, 'required', [])
        for f in self._meta.model._meta.fields:
            if f.name in self.fields:
                required = (f.name in force_required
                            or not (f.null and f.default == NOT_PROVIDED))
                self.fields[f.name].required = required

    class Meta:
        abstract = True


class MixedModelForm(ModelForm):
    """
    This class extends the ability of a ModelForm to allow extra form class
    in relation with the model instance.
    A common usage example is a `User` and a `UserProfile`.
    """
    def __init__(self, *args, **kwargs):
        super(MixedModelForm, self).__init__(*args, **kwargs)
        # retrieve the foreign instance and inject it to its Form.
        for f in self.Meta.extra_forms:
            f_kwargs = deepcopy(kwargs)
            instance = f_kwargs.get('instance', None)
            f_instance = getattr(instance, f[1], None)
            if f_instance:
                f_kwargs['instance'] = f_instance
                f_kwargs['initial'] = kwargs.get('initial', {})
                f_kwargs['initial'].update(model_to_dict(f_instance,
                                           getattr(f[0].Meta, 'fields', []),
                                           getattr(f[0].Meta, 'exclude', [])))
            self.extra_forms = [(f[0](*args, **f_kwargs), f[1])]
        # instantiate each Form and store it to extra_forms with its FK.
        for f in self.extra_forms:
            for field_name in f[0].fields:
                f[0].fields[field_name].initial = (f_kwargs.get('initial', {})
                                                   .get(field_name, None))
            self.fields.update(f[0].fields)

    def full_clean(self, *args, **kwargs):
        super(MixedModelForm, self).full_clean(*args, **kwargs)
        for f in self.extra_forms:
            f[0].full_clean(*args, **kwargs)

    def is_valid(self, *args, **kwargs):
        valid = super(MixedModelForm, self).is_valid(*args, **kwargs)
        valids = [f[0].is_valid() for f in self.extra_forms]
        return valid and not False in valids

    def save(self, *args, **kwargs):
        # save each extra_form and add it to the instance by the FK.
        for f in self.extra_forms:
            foreign_o = f[0].save(*args, **kwargs)
            setattr(self.instance, f[1], foreign_o)
        return super(MixedModelForm, self).save(*args, **kwargs)

    class Meta:
        extra_forms = []  # list of tuples like (FormClassName, instance_fk)


class EditablePasswordFormMixin(object):
    """
    This is a form mixin to allow editing a password or leaving
    the existing one unchanged.
    """
    def __init__(self, *args, **kwargs):
        super(EditablePasswordFormMixin, self).__init__(*args, ** kwargs)
        self.fields['password'] = CharField(required=False,
                                            widget=PasswordInput)
        self.password = None  # Store current user's password

    def clean_password(self):
        password = self.cleaned_data['password']
        if not password:
            self.password = self.instance.password
        return password

    def save(self, *args, **kwargs):
        commit = kwargs.get('commit', True)
        kwargs['commit'] = False
        instance = super(EditablePasswordFormMixin, self).save(*args, **kwargs)
        if self.password:
            # old password to restore (blank provided)
            instance.password = self.password
        else:
            # new password provided
            instance.set_password(self.cleaned_data['password'])
        if commit:
            instance.save()
        return instance
