from django.views.decorators.cache import cache_page
from django.views.generic import TemplateView, RedirectView

from default.decorators import view_decorator

from wkhtmltopdf.views import PDFTemplateView


#@view_decorator(cache_page(60 * 60))
class HomeView(TemplateView):
    template_name = 'index.html'


class RedirectLanguageView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        return "/en/"
