from django.template import TemplateSyntaxError

from .utils import ForceResponse


class ForceResponseMiddleware():
    def process_ion(self, request, e):
        if isinstance(e, TemplateSyntaxError) and getattr(e, 'exc_info', 0):
            try:
                e = e.exc_info[1]
            except:  # Not iterable or IndexError
                raise e  # as if nothing had happened
        if isinstance(e, ForceResponse):
            return e.response
