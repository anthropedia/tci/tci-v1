"""
source from http://djangosnippets.org/snippets/2505/
"""

from django.utils.decorators import method_decorator


def view_decorator(orig_dec):
    """
        Convert the provided decorator to one that can be applied to a view
        class (ie. automatically decorates dispatch)
    """
    method_dec = method_decorator(orig_dec)
    def dec(cls):
        orig_dispatch = cls.dispatch
        def _dispatch(self, *args, **kwargs):
            decorated = method_dec(orig_dispatch)
            return decorated(self, *args, **kwargs)
        cls.dispatch = _dispatch
        return cls
    return dec
