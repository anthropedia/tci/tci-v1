from datetime import date
from django.utils.text import slugify


class ForceResponse(Exception):
    def __init__(self, response):
        self.response = response


class StorageData(object):
    """
    This class stores data and allow managing it.
    ex: storage = StorageData(request.session, 'my_session_key')
    """
    def __init__(self, storage, key_name):
        self.storage = storage
        self.key_name = key_name
        if not key_name in self.storage:
            self.storage[key_name] = {}

    def has(self, key):
        try:
            self.storage.get(self.key_name)[key]
            return True
        except KeyError:
            return False

    def get(self, key=None, default=None):
        d = self.storage.get(self.key_name) or {}
        if key:
            return d.get(key, default)
        return d

    def set(self, key, value):
        d = self.storage.get(self.key_name, {}) or {}
        d[key] = value
        self.storage[self.key_name] = d

    def update(self, data):
        self.storage[self.key_name] = dict(
            self.storage.get(self.key_name, {}).items() + data.items())

    def clear(self):
        self.storage[self.key_name] = {}


def calculate_age(birth):
    today = date.today()
    return (today.year - birth.year -
            ((today.month, today.day) < (birth.month, birth.day)))


def model_slugifier(sender, instance, **kwargs):
    if instance.title:
        instance.slug = slugify(str(instance.title))
