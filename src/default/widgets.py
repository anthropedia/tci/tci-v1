from django.forms.widgets import Input, Widget
from django.utils.html import format_html


class Display(Widget):
    def render(self, name, value=None, attrs=None):
        widget = '<span>{}</span>'.format(value)
        attrs['style'] = 'display:none'
        attrs['type'] = 'text'
        widget += Input().render(name, value, attrs)
        return format_html(widget)
