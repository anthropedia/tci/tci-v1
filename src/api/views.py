import json

from django.http import HttpResponse
from django.views.generic import View
from django.utils.text import slugify
from django.contrib.auth import get_user_model
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.core import serializers

from analyzer.models import Norm
from survey.models import Percentile
from shop.models import TokenCredit


def json_response(func):
    def wrapper(*args, **kwargs):
        resp = func(*args, **kwargs)
        status_code = None
        if type(resp) is tuple and len(resp) > 1 and int(resp[1]) >= 200:
            status_code = resp[1]
            resp = resp[0]
        return HttpResponse(json.dumps(resp), content_type='application/json',
                            status=status_code)
    return wrapper


class NormsView(View):
    @json_response
    def get(self, request):
        data = [
            {
                'survey': slugify(n['trait__questions__survey__title']
                                  or u'default'),
                'country': n['country'].lower(),
                'trait': n['trait__abbreviation'],
                'mean': float(n['mean_value']),
                'deviation': float(n['standard_deviation']),
            }
            for n in Norm.objects.values('trait__abbreviation', 'country',
                                         'mean_value', 'standard_deviation',
                                         'trait__questions__survey__title')
        ]

        return data


class PercentilesView(View):
    @json_response
    def get(self, request):
        data = {}
        for p in Percentile.objects.values('norm__trait__abbreviation',
                                           'norm__country', 'raw_score_min',
                                           'value'):
            country = p['norm__country'].lower()
            trait = p['norm__trait__abbreviation']
            if trait == '00':
                trait = 'default'
            if not data.get(country, None):
                data[country] = {}
            if not data[country].get(trait, None):
                data[country][trait] = {}
            data[country][trait][p['raw_score_min']] = p['value']

        return data


class UserView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(UserView, self).dispatch(request, *args, **kwargs)

    # TODO: secure me. A app token maybe?
    @json_response
    def get(self, request, *args, **kwargs):
        id = int(request.GET.get('id'))

        klass = get_user_model()

        try:
            user = klass.objects.get(pk=id)
            return json.loads(serializers.serialize('json', [user]))
        except klass.DoesNotExist:
            return {'error': 'id'}, 403

    @json_response
    def post(self, request, *args, **kwargs):
        email = request.POST.get('email')
        password = request.POST.get('password')

        klass = get_user_model()
        try:
            user = klass.objects.get(email=email)
            if user.check_password(password):
                return json.loads(serializers.serialize('json', [user]))
            else:
                return {'error': 'password'}, 403
        except klass.DoesNotExist as user:
            return {'error': 'email'}, 403


class TokenCreditView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(TokenCreditView, self).dispatch(request, *args, **kwargs)

    @json_response
    def post(self, request, *args, **kwargs):
        user_id = request.POST.get('user_id')
        key = request.POST.get('token')
        try:
            token = TokenCredit.objects.get(user_id=user_id, key=key)
        except TokenCredit.DoesNotExist:
            return {'error': 'invalid'}, 403
        if not token.is_valid:
            return {'error': 'used',
                    'date': token.usage_date.strftime('%Y-%m-%d')}, 403
        if request.POST.get('use'):
            token.use()
        return {'user_id': token.user_id,
                'token': token.key,
                'type': 'token'}
