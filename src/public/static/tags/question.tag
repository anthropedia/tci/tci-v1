<questions>
    <p>Questions {index + 1} / {total}</p>
    <button onclick={next} in-app></button>

    var tag = this;
    this.index = tag.index || parseInt(opts.index, 10) - 1;
    this.total = parseInt(opts.total, 10);

    this.on('mount', function() {
        // Get the button from the root node, make is clickable and disabled.
        tag.button = tag.root.querySelector('button[in-app]')
        tag.button.disabled = true;
        var buttonReference = tag.root.querySelector('button:not([in-app])');
        tag.button.innerHTML = buttonReference.innerHTML;
        tag.root.removeChild(buttonReference);
    });

    this.on('mount update', function() {
        [].map.call(tag.root.querySelectorAll('ol > li'), function(question, i) {
            // Current question is displayed, clicking a radio enabled the button.
            if(i == tag.index) {
                question.style.display = 'inherit';
                [].map.call(question.querySelectorAll('[type="radio"]'), function(radio) {
                    radio.onclick = function(evt) {
                        tag.button.disabled = false;
                    }
                });
            }
            // Hide other questions
            else {
                question.style.display = 'none';
            }
        });
        if(tag.button) {
            // Hide next button if last question.
            // -2 as we are one round early from the index.
            tag.button.hidden = tag.index > tag.total - 2;
        }
    });

    // When clicking "next button", increment the current question and disable
    // the "next" button.
    next(event) {
        event.preventDefault();
        tag.index++;
        var current = tag.root.querySelectorAll('ol > li')[tag.index];
        // if not answer is selected, disable button
        if(!current.querySelector('input[checked]')) {
            this.button.disabled = true;
        }
    }
</questions>
