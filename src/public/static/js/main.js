$(function($) {
    $('#switch_account select').on('change', function() {
        if (!$(this).val()) return;
        $(this).closest('form').submit();
    });


    // Form submit button should always be visible and sit back when last
    // question is reached.
    var submitButton = $('form [type="submit"]');
    submitButton.addClass('force-inview');
    submitButton.attr('meta-original-label', submitButton.text());
    submitButton.text(submitButton.attr('meta-pause-label'));
    $('#page-surveyform form > ol > li:last-child li:last-child').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            submitButton.removeClass('force-inview');
            submitButton.text(submitButton.attr('meta-original-label'));
        } else {
            submitButton.addClass('force-inview');
            submitButton.text(submitButton.attr('meta-pause-label'));
        }
    });

    var $li, $lis = $('#page-surveyform form > ol > li');
    // Generate an id on each Form question <li>.
    $lis.each(function(i, li) {
        $li = $(li);
        $li.attr('id', 'question-' + $($li.find(':radio').get(0)).attr('name'));
    });
    // If form is in edit mode, find the first empty question and ancor to it.
    if($lis.find(':checked')) {
        $lis.each(function(i, li) {
            $li = $(li);
            if (!$li.find(':checked').get(0)) {
                document.location.href = '#' + $li.attr('id');
                return false;
            }
        });
    }

    // Dashboard / patient update
    $('.result-list tr button').on('click', function() {
        $(this).closest('tr').addClass('selected');
    })
    $('.result-list td:last-child').on('clickoutside', function(evt) {
        $(this).closest('tr').removeClass('selected');
    });

    // Force #id_patients to be filterable
    $('#id_patients label').addClass('name');

    var options = {
      valueNames: [ 'name' ]
    };

    [].forEach.call(document.querySelectorAll('.filterable'), function(filterable) {
      var userList = new List(filterable, options);
    })
});
