import logging

from default import forms
from default import _

from .models import Order, Invoice


logger = logging.getLogger('shop.payment')


class SurveyPurchase1Form(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(SurveyPurchase1Form, self).__init__(*args, **kwargs)
        self.instance.user = self.extra['user']
        self.instance.amount = self.extra['amount']
        self.fields['survey'].queryset = (
            self.fields['survey'].queryset
            .for_profile(self.instance.user.profile))

    class Meta:
        model = Order
        fields = ('survey', 'quantity')


class SurveyPurchase2Form(forms.ModelForm):
    payment_token = forms.CharField(widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        super(SurveyPurchase2Form, self).__init__(*args, **kwargs)
        self.user = self.extra['user']
        self.instance.amount = self.extra['amount']

    def clean_amount(self):
        amount = self.cleaned_data['amount']
        if amount < 100:
            raise forms.ValidationError(_("The amount could not be "
                                          "calculated"))

    class Meta:
        model = Invoice
        fields = ('name', 'street', 'street2', 'city', 'state', 'postal_code',
                  'country')


survey_purchase_forms = [SurveyPurchase1Form, SurveyPurchase2Form]
