from django.contrib.formtools.wizard.views import SessionWizardView
from django.views.generic import DetailView, TemplateView
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages

from default import _

from .forms import SurveyPurchase1Form, SurveyPurchase2Form
from .models import Invoice


class SurveyPurchaseView(SessionWizardView):
    form_list = [SurveyPurchase1Form, SurveyPurchase2Form]

    def dispatch(self, request, **kwargs):
        parent = (super(SurveyPurchaseView, self)
                  .dispatch(request, **kwargs))
        if self.steps.current == '1' and not self.get_amount():
            messages.success(self.request,
                             _("The amount could not be calculated. Check "
                                "with the administrator if the these tests are "
                                "available for your currency."))
            return HttpResponseRedirect(reverse('shop:survey'))
        return parent

    def get_amount(self):
        survey_data = self.get_cleaned_data_for_step('0')
        if survey_data:
            currency = self.request.user.profile.currency
            amount = survey_data['survey'].calculate_price(
                survey_data['quantity'], currency)
        return amount

    def get_currency(self):
        return self.request.user.profile.currency

    def get_filled_forms(self):
        """
        Returns a merged dictionary of all step forms.
        If a step contains a `FormSet`, the key will be prefixed with
        'formset-' and contain a list of the formset forms.
        """
        filled_forms = {}
        for form_key in self.get_form_list():
            form_obj = self.get_form(
                step=form_key,
                data=self.storage.get_step_data(form_key),
                files=self.storage.get_step_files(form_key)
            )
            if form_obj.is_valid():
                if isinstance(form_obj.cleaned_data, (tuple, list)):
                    filled_forms.update({
                        'formset-%s' % form_key: form_obj
                    })
                else:
                    filled_forms.update({form_key: form_obj})
        return filled_forms

    def get_template_names(self):
        templates = ["shop/buy_survey1.html", "shop/buy_survey2.html"]
        return templates[int(self.steps.current)]

    def get_form_kwargs(self, step):
        kwargs = super(SurveyPurchaseView, self).get_form_kwargs(step)
        survey = None
        kwargs.update({
            'extra': {
                'user': self.request.user,
                'currency': self.get_currency(),
                'amount': 0,
            }
        })
        if int(step) == 1:
            survey_data = self.get_cleaned_data_for_step('0')
            if survey_data:
                (survey, quantity) = survey_data.values()
                amount = self.get_amount()
                kwargs['extra'].update({
                    'survey': survey,
                    'amount': int(amount),
                    'quantity': quantity
                })
        return kwargs

    def get_form_initial(self, step):
        initial = super(SurveyPurchaseView, self).get_form_initial(step)
        user = self.request.user
        if int(step) == 1:
            initial.update(user.profile.__dict__)
            initial.update({
                'name': str(user)
            })
        return initial

    def get_context_data(self, form):
        context = super(SessionWizardView, self).get_context_data(form)
        context.update({
            'step': int(self.steps.current),
            'form': form,
        })
        return context

    def done(self, form_list, **kwargs):
        (order_form, invoice_form) = [self.get_filled_forms()[i]
                                      for i in ['0', '1']]
        invoice = invoice_form.save()
        order = order_form.save(commit=False)
        #order.invoice = invoice
        try:
            order.pay(invoice_form.cleaned_data['payment_token'],
                      amount=self.get_amount(),currency=self.get_currency())
        except Exception as e:
            raise e
            pass
            #invoice.delete()
        order.save()

        return HttpResponseRedirect(reverse("shop:survey_done"))


class PurchaseDoneView(TemplateView):
    template_name = "shop/purchase_done.html"

    def get_context_data(self, *args, **kwargs):
        ctx = super(PurchaseDoneView, self).get_context_data(*args, **kwargs)
        ctx['step'] = 2
        return ctx


class InvoiceView(DetailView):
    model = Invoice
