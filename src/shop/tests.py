import json
from datetime import date
from random import randint

from django.test import TestCase
from django.core.urlresolvers import reverse
from django.core.exceptions import ValidationError

from account.models import Coach

from survey.models import Survey

import stripe

from .models import Order, Invoice, SurveyCredit, SurveyPrice


class OrderModelTest(TestCase):

    def setUp(self):
        self.survey = Survey.objects.all()[0]
        self.user = Coach.objects.all()[0]
        self.invoice = Invoice.objects.create(name="John",
                                              street="St Augustin",
                                              postal_code="44000", city="Eden",
                                              country="us", amount=1000)

    def test_new_order_increase_credit(self):
        o = Order(user=self.user, survey=self.survey, quantity=100,
                  invoice=self.invoice)
        o.set_paid({'uid': 123456})
        self.assertTrue(self.user.credits.get(survey=self.survey).
                        amount == 100)
        #
        invoice = Invoice.objects.create(name="John", street="St Augustin",
                                         postal_code="44000", city="Eden",
                                         country="us", amount=1000)
        o = Order(user=self.user, survey=self.survey, quantity=250,
                  invoice=invoice)
        o.set_paid({'uid': 123456})
        self.assertTrue(self.user.credits.get(survey=self.survey).
                        amount == 350)


class InvoiceModelTest(TestCase):

    def setUp(self):
        self.invoice = Invoice.objects.all()[0]
        SurveyPrice.objects.create(quantity=5, price=200,
                                   survey=self.invoice.order.survey)

    def test_bank_data(self):
        data = {'amount': 1500, 'response_code': 1}
        self.invoice.order.set_paid(data)
        self.assertEqual(self.invoice.data, {'amount': 1500,
                                             'response_code': 1})

    def test_quantity_price(self):
        self.assertEqual(self.invoice.survey_price.price, 1000)


class SurveyCreditModelTest(TestCase):

    def setUp(self):
        self.user = Coach.objects.all()[0]
        self.survey = Survey.objects.all()[0]

    def test_decrease_credit(self):
        SurveyCredit.objects.increase(self.user, self.survey, 10)
        SurveyCredit.objects.decrease(self.user, self.survey, 8)
        self.assertEqual(SurveyCredit.objects.get(
            user=self.user, survey=self.survey).amount, 2)

    def test_decrease_credit_not_enough_credit(self):
        SurveyCredit.objects.increase(self.user, self.survey, 10)
        self.assertRaises(ValidationError, SurveyCredit.objects.decrease,
                          self.user, self.survey, 11)


class SurveyPriceModelTest(TestCase):

    def setUp(self):
        self.survey = Survey.objects.get(pk=5)

    def test_get_for_quantity(self):
        self.assertEqual(
            SurveyPrice.objects.get_for_quantity(1, 'usd', self.survey)
            .quantity, 0)
        self.assertEqual(
            SurveyPrice.objects.get_for_quantity(10, 'usd', self.survey)
            .quantity, 10)
        self.assertEqual(SurveyPrice.objects.get_for_quantity(100, 'usd',
                         self.survey).quantity, 50)


class StripeTest(TestCase):

    def setUp(self):
        self.card = {
            'number': '4242424242424242',
            'exp_month': date.today().month,
            'exp_year': date.today().year + 4
        }

    def test_basic_payment(self):
        charge = stripe.Charge.create(amount=400, currency="usd",
                                      card=self.card)
        self.assertDictContainsSubset(json.dumps(charge),
                                      {"amount": 400, "paid": True})


class BuySurveyViewTest(TestCase):

    def setUp(self):
        self.user = Coach.objects.get(email="coach@tci.com")
        self.client.login(email="coach@tci.com", password="coach")

    def pass_step1(self, data={}):
        return self.client.post(reverse('shop:survey') + "?step=0", dict({
            'survey': 5,
            'quantity': 3
        }.items() + data.items()), follow=True)

    def pass_step2(self, data={}):
        return self.client.post(reverse('shop:survey') + "?step=1", dict({
            'name': "John Doe",
            'street': "St-Louis street",
            'street2': "Appt 3",
            'city': "San Fransisco",
            'postal_code': "94109",
            'state': "CA",
            'country': "US",
        }.items() + data.items()), follow=True)

    def pass_step3(self, data={}):
        return self.client.post(reverse('shop:survey') + "?step=2", dict({
            'card_number': "4111111111111111",
            'exp_month': "01",
            'exp_year': date.today().year + 1,
            'cvv': randint(1, 999),
            'first_name': "Donery",
            'last_name': "Jonathan"
        }.items() + data.items()), follow=True)

    def test_regular_purchase(self):
        r = self.pass_step1()
        r = self.pass_step2()
        try:
            r = self.pass_step3()
            self.assertTemplateUsed(r, 'shop/buy_survey_done.html')
            invoice = Invoice.objects.all()[0]
            self.assertEqual(invoice.amount, 210.0)
            self.assertDictContainsSubset(
                {'amount': "210.00", 'response_code': "1"}, invoice.data)

            credit = SurveyCredit.objects.get(user=self.user,
                                              survey__pk=5)
            self.assertEqual(credit.amount, 3)
        except Exception as e:
            self.skipTest(e)

    def test_fail_step1(self):
        r = self.pass_step1({'survey': 999})
        self.assertTemplateUsed(r, 'shop/buy_survey0.html')
        self.assertEqual(r.status_code, 200)

    def test_survey_no_price(self):
        Survey.objects.get(pk=5).surveyprice_set.delete()
        r = self.pass_step1()
        self.assertTemplateUsed(r, 'shop/buy_survey0.html')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(len(list(r.context['messages'])), 1)
        self.assertEqual(str(list(r.context['messages'])[0]),
                         'shop.error.amount')

    def test_fail_payment(self):
        self.pass_step1()
        self.pass_step2()
        r = self.pass_step3({'card_number': "", 'exp_month': "",
                             'exp_year': "2000"})
        self.assertTemplateUsed(r, 'shop/buy_survey2.html')
        self.assertEqual(r.status_code, 200)
