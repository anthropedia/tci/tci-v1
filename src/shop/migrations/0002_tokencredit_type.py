# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='tokencredit',
            name='type',
            field=models.CharField(choices=[('research', 'Research/Study'), ('tci3240', 'TCI 3 240')], max_length=50, default='research'),
            preserve_default=False,
        ),
    ]
