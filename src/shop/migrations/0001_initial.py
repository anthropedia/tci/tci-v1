# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings
import django_localflavor_us.models
import django_countries.fields
import model_utils.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('survey', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Invoice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('street', models.CharField(max_length=50)),
                ('street2', models.CharField(null=True, max_length=50)),
                ('city', models.CharField(max_length=50)),
                ('state', django_localflavor_us.models.USStateField(choices=[('AL', 'Alabama'), ('AK', 'Alaska'), ('AS', 'American Samoa'), ('AZ', 'Arizona'), ('AR', 'Arkansas'), ('AA', 'Armed Forces Americas'), ('AE', 'Armed Forces Europe'), ('AP', 'Armed Forces Pacific'), ('CA', 'California'), ('CO', 'Colorado'), ('CT', 'Connecticut'), ('DE', 'Delaware'), ('DC', 'District of Columbia'), ('FL', 'Florida'), ('GA', 'Georgia'), ('GU', 'Guam'), ('HI', 'Hawaii'), ('ID', 'Idaho'), ('IL', 'Illinois'), ('IN', 'Indiana'), ('IA', 'Iowa'), ('KS', 'Kansas'), ('KY', 'Kentucky'), ('LA', 'Louisiana'), ('ME', 'Maine'), ('MD', 'Maryland'), ('MA', 'Massachusetts'), ('MI', 'Michigan'), ('MN', 'Minnesota'), ('MS', 'Mississippi'), ('MO', 'Missouri'), ('MT', 'Montana'), ('NE', 'Nebraska'), ('NV', 'Nevada'), ('NH', 'New Hampshire'), ('NJ', 'New Jersey'), ('NM', 'New Mexico'), ('NY', 'New York'), ('NC', 'North Carolina'), ('ND', 'North Dakota'), ('MP', 'Northern Mariana Islands'), ('OH', 'Ohio'), ('OK', 'Oklahoma'), ('OR', 'Oregon'), ('PA', 'Pennsylvania'), ('PR', 'Puerto Rico'), ('RI', 'Rhode Island'), ('SC', 'South Carolina'), ('SD', 'South Dakota'), ('TN', 'Tennessee'), ('TX', 'Texas'), ('UT', 'Utah'), ('VT', 'Vermont'), ('VI', 'Virgin Islands'), ('VA', 'Virginia'), ('WA', 'Washington'), ('WV', 'West Virginia'), ('WI', 'Wisconsin'), ('WY', 'Wyoming')], null=True, max_length=2)),
                ('postal_code', models.CharField(max_length=10)),
                ('country', django_countries.fields.CountryField(max_length=2)),
                ('amount', models.PositiveIntegerField(default=0)),
                ('bank_data', models.TextField(null=True)),
                ('creation_date', models.DateField(auto_now=True)),
            ],
            options={
                'ordering': ('-creation_date',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', model_utils.fields.StatusField(verbose_name='status', default='pending', no_check_for_status=True, max_length=100, choices=[('pending', 'pending'), ('paid', 'paid')])),
                ('status_changed', model_utils.fields.MonitorField(verbose_name='status changed', default=django.utils.timezone.now, monitor='status')),
                ('quantity', models.PositiveIntegerField()),
                ('discount_code', models.CharField(max_length=20)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('invoice', models.OneToOneField(related_name='order', to='shop.Invoice', null=True)),
                ('survey', models.ForeignKey(related_name='order_set', to='survey.Survey')),
                ('user', models.ForeignKey(related_name='order_set', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-creation_date',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SurveyCredit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.PositiveIntegerField(default=0)),
                ('survey', models.ForeignKey(to='survey.Survey')),
                ('user', models.ForeignKey(related_name='credit_set', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SurveyPrice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity', models.PositiveIntegerField()),
                ('price', models.PositiveIntegerField(help_text='Price in cents! 1500=$15')),
                ('currency', models.CharField(default='usd', max_length=3, choices=[('usd', 'US Dollars'), ('eur', 'Euros')])),
                ('survey', models.ForeignKey(to='survey.Survey')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TokenCredit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', models.CharField(unique=True, max_length=32)),
                ('name', models.CharField(null=True, max_length=100)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('usage_date', models.DateTimeField(null=True)),
                ('price', models.PositiveIntegerField(null=True)),
                ('comment', models.TextField(null=True)),
                ('user', models.ForeignKey(related_name='token_set', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='surveycredit',
            unique_together=set([('user', 'survey')]),
        ),
    ]
