from django.contrib import admin

from default import forms
from .models import Invoice, TokenCredit


class InvoiceAdmin(admin.ModelAdmin):
    readonly_fields = ('creation_date', 'order_data')

    def order_data(self, invoice):
        o = invoice.order
        return (u"test: {survey}, quantity: {quantity}".format(
            survey=o.survey, quantity=o.quantity))

    class Meta:
        model = Invoice


class TokenCreditAdminForm(forms.ModelForm):
    pass


class TokenCreditAdmin(admin.ModelAdmin):
    form = TokenCreditAdminForm
    fields = ['user', 'type', 'name', 'price', 'comment', 'key', 'usage_date']
    readonly_fields = ['usage_date', 'key']
    list_display = ['user', 'type', 'key', 'usage_date']
    ordering = ['-creation_date']
    search_fields = ['user__lastname', 'user__firstname', 'comment', 'key']

    class Meta:
        model = TokenCredit


admin.site.register(TokenCredit, TokenCreditAdmin)

admin.site.register(Invoice, InvoiceAdmin)
