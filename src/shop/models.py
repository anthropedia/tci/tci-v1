import json
import stripe
import hashlib
import datetime

from django.db import models
from django.db.models.query import QuerySet
from django.core.exceptions import ValidationError
from django.dispatch import receiver
from django.conf import settings

from jsonfield import JSONField

from model_utils import Choices
from model_utils.models import StatusModel
from model_utils.managers import PassThroughManager
from django_countries.fields import CountryField
from django_localflavor_us.models import USStateField

from account import constants


class Order(StatusModel):
    STATUS = Choices('pending', 'paid')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='order_set')
    survey = models.ForeignKey('survey.Survey', related_name='order_set')
    quantity = models.PositiveIntegerField()
    discount_code = models.CharField(max_length=20)
    creation_date = models.DateTimeField(auto_now_add=True)
    invoice = models.OneToOneField('Invoice', related_name='order', null=True)
    #invoice_data = JSONField(null=True, default="")
    just_paid = False  # Flag to make sure it's a new payment.

    def pay(self, token, amount, currency):
        stripe.api_key = settings.STRIPE_SECRET_KEY
        self.save()  # run a first fake save validation
        charge = stripe.Charge.create(
            amount=amount,  # in cents (no decimal)
            currency=currency,
            card=token,
            description=self.user.email,
            receipt_email=self.user.email
        )
        self.just_paid = True
        self.status = 'paid'
        self.save()
        #self.invoice.bank_data = json.dumps(charge)
        #self.invoice.save()

    def __str__(self):
        if self.survey and self.user:
            return "%s (%s)" % (self.survey, self.user)
        else:
            return str(self.pk)

    class Meta:
        ordering = ('-creation_date',)


@receiver(models.signals.post_save, sender=Order)
def order_post_save(sender, instance, **kwargs):
    if instance.just_paid:
        instance.just_paid = False
        SurveyCredit.objects.increase(instance.user, instance.survey,
                                      instance.quantity)


class Invoice(models.Model):
    name = models.CharField(max_length=100)
    street = models.CharField(max_length=50)
    street2 = models.CharField(max_length=50, null=True)
    city = models.CharField(max_length=50)
    state = USStateField(null=True)
    postal_code = models.CharField(max_length=10)
    country = CountryField()
    amount = models.PositiveIntegerField(default=0)  # in cents.
    bank_data = models.TextField(null=True)
    creation_date = models.DateField(auto_now=True)

    def __str__(self):
        try:
            o = self.order
            return "%s - %s" % (self.reference, o.survey.title)
        except Order.DoesNotExist:
            return str(self.pk)

    @property
    def survey_price(self):
        """return a SurveyPrice instance based on
           the order survey and quantity"""

        return SurveyPrice.objects.get_for_quantity(self.order.quantity,
                                                    self.order.user.currency,
                                                    self.order.survey)

    @property
    def data(self):
        return json.loads(self.bank_data)

    @property
    def currency(self):
        """
        XXX: this should be a concrete property.
        """
        return self.order.user.profile.currency

    @property
    def user(self):
        return self.order.user

    @property
    def reference(self):
        return str(self.id).zfill(5)

    def save(self, *args, **kwargs):
        return super(Invoice, self).save(*args, **kwargs)
        #self.order.invoice_data = self.__dict__
        self.order.save(kwargs['commit'])

    class Meta:
        ordering = ('-creation_date',)


class SurveyCreditQuerySet(QuerySet):
    def increase(self, user, survey, amount):
        credit = self.get_or_create(user=user, survey=survey)[0]
        credit.amount += amount
        credit.save()

    def decrease(self, user, survey, amount):
        credit = self.get_or_create(user=user, survey=survey)[0]
        credit.amount -= amount
        if credit.amount >= 0:
            credit.save()
        else:
            raise ValidationError("User %s has less than %s amount for %s" %
                                  (user, amount, survey))


class SurveyCredit(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             related_name='credit_set')
    survey = models.ForeignKey('survey.Survey')
    amount = models.PositiveIntegerField(default=0)

    objects = PassThroughManager.for_queryset_class(SurveyCreditQuerySet)()

    def __str__(self):
        return "%s - %s: %d credits" % (self.user, self.survey,
                                        self.amount / 100)

    class Meta:
        unique_together = ('user', 'survey')


class SurveyPriceQuerySet(QuerySet):

    def get_for_quantity(self, quantity, currency='usd', survey=None):
        try:
            qs = (self.filter(quantity__lte=quantity)
                  .filter(currency=currency.lower())
                  .order_by('-quantity'))
            if survey:
                qs = qs.filter(survey=survey)
            return qs[0]
        except IndexError:
            return None


class SurveyPrice(models.Model):
    survey = models.ForeignKey('survey.Survey')
    quantity = models.PositiveIntegerField()
    price = models.PositiveIntegerField(help_text="Price in cents! 1500=$15")
    currency = models.CharField(max_length=3,
                                choices=(constants.CURRENCIES),
                                default=constants.CURRENCIES[0][0])

    objects = PassThroughManager.for_queryset_class(SurveyPriceQuerySet)()

    def __str__(self):
        return "%s %d+" % (self.survey, self.quantity)


class TokenCredit(models.Model):
    token_types = [
        ('research', 'Research/Study'),
        ('tci3240', 'TCI 3 240'),
    ]
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             related_name='token_set')
    key = models.CharField(max_length=32, unique=True)
    name = models.CharField(max_length=100, null=True)
    type = models.CharField(max_length=50, choices=token_types)
    creation_date = models.DateTimeField(auto_now_add=True)
    usage_date = models.DateTimeField(null=True)
    price = models.PositiveIntegerField(null=True)
    comment = models.TextField(null=True)

    def __str__(self):
        return str(self.key)

    @property
    def is_valid(self):
        return self.usage_date is None

    def invalidate(self):
        self.usage_date = datetime.datetime.now()
        self.save()

    def use(self):
        """
        @deprecated: use `invalidate()` instead.
        """
        return self.invalidate()


@receiver(models.signals.post_save, sender=TokenCredit)
def generate_token(sender, instance, **kwargs):
    if instance.key:
        return
    sha = hashlib.md5(str(str(instance.user.id) + instance.name +
                          str(instance.creation_date.timestamp())
                      ).encode('utf-8'))
    instance.key = sha.hexdigest()
    instance.save()
