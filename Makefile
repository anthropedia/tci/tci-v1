branch=master
git_update=git fetch origin ${branch} && git reset --hard FETCH_HEAD
goto_root=cd ~/tci-v1 && . ./venv/bin/activate

help:
	return "Make tasks for deployment. Checkout the makefile content."

server_update:
	@echo '> Pulling from ${branch}'
	ssh tci "${goto_root} && ${git_update}"
	ssh tci "${goto_root} && pip install -r requirements.txt"
	ssh tci "${goto_root} && python src/manage.py migrate --settings=core.settings.prod"
	ssh tci "sudo systemctl restart tci-v1"

deploy_prod: server_update
