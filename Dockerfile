FROM python:2.7-alpine
RUN apk add gcc musl-dev mariadb-connector-c-dev && \
    apk add bash git libc-dev mariadb-dev
COPY . /app
WORKDIR /app
RUN pip install gunicorn -r requirements.txt

ENTRYPOINT ["sh", "entrypoint.sh"]
