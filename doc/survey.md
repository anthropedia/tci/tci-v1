# survey

## Run a Survey

### As a Patient

1. select a `Survey` assigned from my coach that appears on my dashboard
2. answer the `Question`s and submit
3. if complete, I read a confirmation and my `Survey` changes status. It does
appear anymore on ny dashboard and my coach can access my results
4. if not complete, I get redirected to my dashboard with an uncomplete message


## Traits trees are broken

Traits use Mppt Nestedset to manage their hiercarchy.
It happens that it break (I know…). It that case simply run in a Django shell:
```
from survey.models import Trait
Trait.objects.rebuild()
```
