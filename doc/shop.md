# shop

A coach can buy a certain quantity of `Survey` in once.

The purpose of buying `Survey`s is to increase `SurveyCredit` for a certain type
of `Survey`.

One `SurveyCredit` can be spent to give access to one `Patient` to a `Survey`.


### Purchase process

1. select survey and quantity
2. insert your billing informations
3. create an `Order` with _pending_ status
3. post data to bank for payment process
4. bank posts data back to us and we set the `Order` to _paid_ status
5. the _coach_ can see his `SurveyCredit` increased


### Spending `SurveyCredit`

1. create a `CoachProgram` for a `Survey`
2. assign multiple `Patient`s and save. That decreases the `SurveyCredit`.

