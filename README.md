# Anthropedia TCI

Here are the source for the TCI project.
This is written in Python based on latest Django stable (1.5.1).


## Installation

Below is the description for running manually.
You can also use Docker/Docker Compose.

You should get an export of the database and import it:

```
docker compose exec -iT db mysql -e "create database tci;" 
docker compose exec -iT db mysql tci < my_database_dump.sql
```

### Requirements

- Python 2.6 or 2.7 (unknown for higher version)
- virtualenv

For development:

- Compass and Sass (requires Ruby for compiling Sass to CSS)
- Install Zurb-foundation dependencies:

    gem install zurb-foundation
 

### Local, for development

    virtualenv venv
    pip install -r requirements_local.txt

### On a server, for production

    virtualenv venv
    pip install -r requirements.txt


## Configuration

The settings file is splitted for environment overriding:

- __core/settings/base.py__ is the default values file.
- __core/settings/local.py__ is the local dev configuration file.
- __core/settings/staging.py__ is the file for staging server.
- __core/settings/prod.py__ is the file for production.


## Running the project

### In local

	source ./venv/bin/activate
	fab runserver

> Note that `fab runserver` attempts to compile Sass assets and therefore requires
> `compass` binary to be callable.

### On a server

create a dedicated _wsgi.py_ file that refers to the appropriate settings
file as seen in the _Configuration_ section here above.


## Specific notes

- The templates use [jinja2](http://jinja.pocoo.org/) via [Jingo](https://jingo.readthedocs.org/en/latest/)
instead of the default Django templates.
- [Foundation](http://foundation.zurb.com) (v4.3.1) is the frontend CSS framework
partially used. Based on [Sass](http://sass-lang.com/) (for pre-processing) with [Compass](http://compass-style.org/).
Also [Pleeease](http://pleeease.io) is used for post-processing
(necessary for wkhtmltopdf with CSS3 due to the old embedded version of webkit).
- Permissions are managed by user class and each list is managed in account.permissions.
