import os
import shutil
import site
site.addsitedir(os.path.dirname(__file__) + "/src")


from fabric.api import local, env, cd, run, prefix

from core.settings import local as settings_local
from core.settings import staging as settings_staging
from core.settings import prod as settings_prod


# Server conf
env.use_ssh_config = True
env.hosts = ["anthropedia"]

dev = {
    'name': 'dev',
    'directory': os.path.dirname(__file__),
    'static_directory': os.path.dirname(__file__) + "/src/public/static",
    'activate': "source ~/.virtualenvs/tci/bin/activate",
    'settings': " --settings=core.settings.local",
    'mysql': settings_local.DATABASES['default'],
    'command': local,
}
staging = {
    'name': 'staging',
    'directory': "~/webapps/tci_staging/repo",
    'static_directory': '~/webapps/tci_staging_static',
    'activate': "workon tci-staging",
    'settings': " --settings=core.settings.staging",
    'apache': "~/webapps/tci_staging/apache2/bin/restart",
    'mysql': settings_staging.DATABASES['default'],
    'branch': "master",
    'command': run,
}
prod = {
    'name': 'prod',
    'directory': "~/webapps/tci_retro/repo",
    'static_directory': "~/webapps/tci_retro_static",
    'activate': "workon tci-retro",
    'settings': " --settings=core.settings.prod",
    'apache': "~/webapps/tci_retro/apache2/bin/restart",
    'mysql': settings_prod.DATABASES['default'],
    'branch': "prod",
    'command': run,
}


def deploy(fast=False, env='staging'):
    env = globals()[env]
    with cd(env['directory']):
        run("git fetch origin {branch}".format(**env))
        run("git reset --hard FETCH_HEAD")
    if fast:
        _restart_server(env)
    else:
        _update_env(env)


def _restart_server(env):
    run(env['apache'])


def _update_env(env):
    with cd(env['directory']):
        #assets_build(env)
        with prefix(env['activate']):
            run("pip install --upgrade -r requirements.txt")
            run("python src/manage.py migrate --noinput" + env['settings'])
            run("python src/manage.py collectstatic --noinput"
                + env['settings'])
            _restart_server(env)


def assets_watch():
    local("compass watch public/static/sass & >& /dev/null")


def runserver():
    assets_watch()
    with prefix(dev['activate']):
        local("python manage.py runserver" + dev['settings'])


def update_prod():
    with cd(prod['directory']):
        run("git fetch origin {branch}; git reset --hard FETCH_HEAD"
            .format(**prod))
    _update_env(prod)


def download_staging():
    #run("""mysql -u "{user}" tci -e "drop database tci;
    #    create database tci;" """
    #    .format(user=dev['mysql']['USER'], pwd=dev['mysql']['PASSWORD']))
    run("""mysqldump -u "{user}" --password="{pwd}" tcistaging > \
        ~/backups/tcistaging.`date "+%Y%m%d%H%M"`.sql""".format(
        user=staging['mysql']['USER'], pwd=staging['mysql']['PASSWORD']))
    local("rsync -avz {host}:~/backups/"
          "`ssh {host} 'ls -t ~/backups/ | head -1'`"
          " ../backups/".format(host=env.hosts[0]))
    local("""mysql -u "{user}" tci < ../backups/`ls -t ../backups | head -1`"""
          .format(user=dev['mysql']['USER']))


def download_prod():
    run("""mysqldump -u "{user}" --password="{pwd}" tciprod > \
        ~/backups/tciprod.`date "+%Y%m%d%H%M"`.sql""".format(
        user=prod['mysql']['USER'], pwd=prod['mysql']['PASSWORD']))
    local("rsync -avz {host}:~/backups/"
          "`ssh {host} 'ls -t ~/backups/ | head -1'`"
          " ../backups/".format(host=env.hosts[0]))
    local("""mysql -u "{user}" tci < ../backups/`ls -t ../backups | head -1`"""
          .format(user=dev['mysql']['USER']))


def assets_preprocess(env=dev):
    command = env['command']
    command("rm -r {0}".format(env['directory'] + "/src/public/static/sass"))
    command("compass compile %s%s" % (env['directory'],
                                      '/src/public/static/sass'))


def assets_postprocess(env=dev):
    source_path = env['directory'] + "/src/public/static/css"
    target_path = env['static_directory'] + '/css'
    tmp_path = '/tmp/css'
    files = ["app", "error", "printable", "printable.print", "result_3d"]
    command = env['command']

    command("cp -R {source_path} {tmp_path}".format(**locals()))
    for f in files:
        command("pleeease compile {tmp_path}/{f}.css -t {target_path}/{f}.css"
                .format(**locals()))
    #command("rm -r {tmp_path}".format(**locals()))


def assets_build(env=dev):
    assets_preprocess(env)
    assets_postprocess(env)
