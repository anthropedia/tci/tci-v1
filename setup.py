from distutils.core import setup


setup(
    name='tciold',
    version='1.0',
    author='Vincent Agnano',
    license='Copyright Anthropedia',
    long_description=open('./README.md').read(),
    package_dir={'': 'src'},
    install_requires=[
      'Django==1.7.11',
      'django-countries==3.1.1',
      'django-debug-toolbar==1.2.2',
      'django-extensions==1.4.9',
      'django-grappelli==2.6.3',
      'django-localflavor-us==1.1',
      'django-model-utils==2.2',
      'django-mptt==0.6.1',
      'django-mptt-admin==0.2.0',
      'django-transadmin',
      # 'Fabric==1.10.1',
      # 'ipython==2.3.1',
      'jingo==0.7',
      'Jinja2==2.7.3',
      'jsonfield==1.0.0',
      'markdown',
      'MarkupSafe==0.23',
      'requests==2.5.1',
      'stripe==1.20.1',
      'mysqlclient',
    ]
)
