## 2015-01-25 data migration for accouts

- The old data should migration to the new schema running `fab migrate`
- when migrations will be applied on prod we should clean them and remove _account/migrations/models_.


## 2015-01-21 shop goes in cents instead of full units

For the old system to be compatible:
- change the amounts on the admin panel to match cents


## 2015-01-19 account system has changed

For the old system to be compatible:
- restore coach/researcher/patient to the new account system
- restore patient <> professional relationship
- restore patient <> survey results
- restore the credits for each professional
