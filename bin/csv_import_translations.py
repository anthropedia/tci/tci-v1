#!/usr/bin/env python
# -*- coding: utf_8 -*-
"""
Import a CSV language file.
Columns must be <tab> seperated in this format with no header:
<source>  <translation in English>  <translation in French>
"""
import csv
import os
import sys

from codecs import open
from django.core.wsgi import get_wsgi_application


try:
    command, filename = sys.argv
except ValueError:
    raise ValueError("This command requires 2 arguments: a translation "
                     "language and a CSV file path. %d arguments were passed."
                     % (len(sys.argv) - 1))

sys.path.append(os.path.dirname(__file__)+'/../src')
os.environ.setdefault("DJANGO_SETTINGS_MODULE",
                      os.environ.get('SETTINGS') or "core.settings.local")
application = get_wsgi_application()

from transadmin.models import Translation


c = csv.reader(open(filename, "rU"), delimiter="\t", dialect=csv.excel_tab)

count = 0

for row in c:
    # language = 'en'
    # t, c = Translation.objects.get_or_create(context='survey_test',
    #                                         language=language, source=row[0])
    # t.trans = row[1]
    # t.save()

    language = 'fr'
    t, c = Translation.objects.get_or_create(context='survey_test',
                                             language=language, source=row[0])
    #print row
    t.trans = row[1]
    t.save()
    count += 1

print "imported %d translations in %s" % (count, language)
