# 20131109 - using context and comment

from django.db import IntegrityError

from transadmin.models import Translation


apps = ('account', 'shop', 'survey')

subcontexts = (
    ('patient_create', 'account'),
    ('patient_update', 'account'),
    ('login', 'account'),
    ('coach_dashboard', 'account'),
    ('client_dashboard', 'account'),
)


def move_context_to_comment(t):
    if t.comment:
        t.comment += "\n%s" % t.context
    else:
        t.comment = t.context
    t.context = None


def has_app_context(t, apps):
    prefixes = tuple(app+"." for app in apps)
    if t.source.startswith(prefixes):
        return True
    return False


def move_prefix_to_context(t, apps):
    prefixes = tuple(app+"." for app in apps)
    if t.source.startswith(prefixes):
        t.context = t.source[:t.source.index(".")]
        t.source = t.source[t.source.index(".")+1:]


def guess_context_from_subcontext(t, subcontexts):
    if t.context:
        return
    prefixes = tuple(c[0]+"." for c in subcontexts)
    if t.source.startswith(prefixes):
        prefix = t.source[:t.source.index(".")]
        t.context = dict(subcontexts)[prefix]
        t.source = t.source[t.source.index(".")+1:]


for t in Translation.objects.all():
    if not t.context in apps:
        move_context_to_comment(t)
        if has_app_context(t, apps):
            move_prefix_to_context(t, apps)
    if not t.context:
        guess_context_from_subcontext(t, subcontexts)
    if t.context == "":
        t.context = None
    try:
        t.save()
    except IntegrityError, e:
        print t, "<<>>", e
