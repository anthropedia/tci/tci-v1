#!/usr/bin/env python
# -*- coding: utf_8 -*-
"""
Import a CSV language file.
Columns must be <tab> seperated in this format with no header:
<source>  <translation in English>  <translation in French>
"""
import csv
import os
import sys

from codecs import open
from django.core.wsgi import get_wsgi_application

# django bootstrap
PATH = os.path.dirname(__file__)
sys.path.append(PATH + '/../../src')
os.environ.setdefault("DJANGO_SETTINGS_MODULE",
                      os.environ.get('SETTINGS') or "core.settings.local")
application = get_wsgi_application()


from transadmin.models import Translation

filename = PATH + '/questions.csv'
c = csv.reader(open(filename, "rU"), delimiter=";", dialect=csv.excel_tab)
language = 'sw'

count = 0
for row in c:
    t, c = Translation.objects.get_or_create(context='survey_test',
                                             language=language, source=row[0])
    t.trans = row[1]
    t.save()
    # For debug purpose
    count += 1

print "imported %d translations in %s" % (count, language)
