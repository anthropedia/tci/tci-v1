#!/usr/bin/env python
import csv
import os
import sys


try:
    command, title, filename = sys.argv
except ValueError:
    raise ValueError("This command requires 2 arguments: a survey title and "
                     "a CSV file path. %d arguments were passed"
                     % (len(sys.argv) - 1))

sys.path.append(os.path.dirname(__file__) + '/../src')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core.settings.local")


from survey.models import Survey, Question, Answer, Trait

c = csv.reader(open(filename, "rU"), delimiter=";",
               dialect=csv.excel_tab)


survey, created = Survey.objects.get_or_create(title=title)

for i, row in enumerate(c):
    try:
        if row[1]:
            trait = Trait.objects.get(abbreviation=row[1].lower())
        else:
            trait = None
        q, created = Question.objects.get_or_create(survey=survey, title=row[0],
                                                    position=i, trait=trait)
        Answer.objects.get_or_create(question=q, title="answer.false",
                                     value=row[2], position=0)
        Answer.objects.get_or_create(question=q, title="answer.most_false",
                                     value=row[3], position=1)
        Answer.objects.get_or_create(question=q, title="answer.neither",
                                     value=row[4], position=2)
        Answer.objects.get_or_create(question=q, title="answer.most_true",
                                     value=row[5], position=3)
        Answer.objects.get_or_create(question=q, title="answer.true", value=row[6],
                                     position=4)
    except Exception, e:
        raise Exception("error for row%d (%s): %s" % (i, row, e))
